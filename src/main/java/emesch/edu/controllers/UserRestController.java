package emesch.edu.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import emesch.edu.Api;
import emesch.edu.Utils;
import emesch.edu.data.ElementType;
import emesch.edu.data.ResponseData;
import emesch.edu.data.ResponseDataElementKeys;
import emesch.edu.data.ResponseElement;
import emesch.edu.data.ResponseView;
import emesch.edu.data.error.ResponseError;
import emesch.edu.model.users.JwtUser;
import emesch.edu.model.users.User;
import emesch.edu.model.users.UserProfile;
import emesch.edu.security.JwtTokenUtil;
import emesch.edu.services.UserService;

@RestController
public class UserRestController {

	private UserService userService;
	private JwtTokenUtil jwtTokenUtil;	
	private UserDetailsService userDetailsService;	

	@Autowired
	public UserRestController(UserService userService, JwtTokenUtil jwtTokenUtil, UserDetailsService userDetailsService) {
		this.userService = userService;
		this.jwtTokenUtil = jwtTokenUtil;
		this.userDetailsService = userDetailsService;		
	}

	@RequestMapping(value = Api.PUBLIC_USER_PROFILE+"/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResponseData> getPublicProfile(@PathVariable("id") long profileId, HttpServletRequest request) {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();
		UserProfile publicProfile = this.userService.getPublicUserProfile(profileId);		
		List<ResponseError> responseErrors = new ArrayList<>();
		if(publicProfile != null) {
			status = HttpStatus.OK;	
			responseData.setView(ResponseView.PUBLIC_PROFILE);
			responseData.setElements(publicProfile.getResponseElements());
			responseData.setErrors(responseErrors);
		}else {
			status = HttpStatus.NOT_FOUND;
			responseData.setView(ResponseView.USER_NOT_FOUND);			
		}		
		return new ResponseEntity<>(responseData, status);
	}

	@RequestMapping(value = Api.PUBLIC_GET_FRIENDS, method = RequestMethod.GET)
	public ResponseEntity<ResponseData> getFriends(@PathVariable("id") long userId, HttpServletRequest request) {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();
		Map<String, ResponseElement> friendsInfoMap = this.userService.getAllInfoAboutFriendsOfUser(userId);
		List<ResponseError> responseErrors = new ArrayList<>();
		if(friendsInfoMap != null) {
			status = HttpStatus.OK;	
			responseData.setView(ResponseView.FRIENDS_OF_USER);
			responseData.setElements(friendsInfoMap);
			responseData.setErrors(responseErrors);
		}else {
			status = HttpStatus.NOT_FOUND;						
		}		
		return new ResponseEntity<>(responseData, status);		
	}

	@RequestMapping(value = Api.PUBLIC_REGISTER_USER, method = RequestMethod.POST)
	public ResponseEntity<ResponseData> registerUser(@RequestParam String username, @RequestParam String email, @RequestParam String password) {
		HttpStatus status = null;		
		ResponseData responseData = new ResponseData();
		try {
			//TODO: User enabled policy
			User createdUser = this.userService.registerUser(username, email, password);			
			status = HttpStatus.CREATED;			
			responseData.setView(ResponseView.USER_REGISTERED);
			responseData.addElement(ResponseDataElementKeys.REGISTERED_USER_ID, new ResponseElement(ElementType.USER_ID, createdUser.getId()));
		} catch (DuplicateKeyException e) {
			status = HttpStatus.CONFLICT;
		}		

		return new ResponseEntity<>(responseData, status);
	}		

	@RequestMapping(value = Api.PRIVATE_GET_CURRENT_USER, method = RequestMethod.GET)
	public JwtUser getAuthenticatedUser(HttpServletRequest request) {
		JwtUser user = null;		
		String token = Utils.getTokenFromRequest(request);

		String username = jwtTokenUtil.getUsernameFromToken(token);
		user = (JwtUser) userDetailsService.loadUserByUsername(username);			

		return user;
	}

	@RequestMapping(value = Api.PRIVATE+"/{id}", method = RequestMethod.GET)
	public ResponseEntity<ResponseData> getPrivateContent(@PathVariable("id") long requestId, HttpServletRequest request) {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();
		String token = Utils.getTokenFromRequest(request);
		long currentUserId = jwtTokenUtil.getUserIdFromToken(token, userService);		
		if(currentUserId == requestId) {
			status = HttpStatus.OK;	
			responseData.setView(ResponseView.PRIVATE_CONTENT);
		}else {
			status = HttpStatus.UNAUTHORIZED;
			responseData.setView(ResponseView.NO_PERMISSION);
		}		
		return new ResponseEntity<>(responseData, status);
	}		

}



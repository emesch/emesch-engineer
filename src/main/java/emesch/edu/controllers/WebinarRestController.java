package emesch.edu.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import emesch.edu.Api;
import emesch.edu.Utils;
import emesch.edu.data.ElementType;
import emesch.edu.data.ResponseData;
import emesch.edu.data.ResponseDataElementKeys;
import emesch.edu.data.ResponseElement;
import emesch.edu.data.ResponseView;
import emesch.edu.data.error.ResponseError;
import emesch.edu.data.error.ResponseErrorEnum;
import emesch.edu.model.json.JsonNewWebinar;
import emesch.edu.model.webinar.Webinar;
import emesch.edu.security.JwtTokenUtil;
import emesch.edu.services.UserService;
import emesch.edu.services.WebinarService;

@RestController
public class WebinarRestController {
	
	public static Logger logger = Logger.getLogger(WebinarRestController.class.getName());
	
	private UserService userService;
	private WebinarService webinarService;

	
	@Autowired
	public WebinarRestController(UserService userService, WebinarService webinarService) {
		this.userService = userService;
		this.webinarService = webinarService;
	}
	
	@RequestMapping(value=Api.GET_WEBINARS_ONLINE)
	public ResponseEntity<ResponseData> getWebinarsOnline() {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();			
		List<ResponseError> responseErrors = new ArrayList<>();
		ResponseElement webinarsOnline = this.webinarService.getWebinarsOnlineAsResponseElement();
		if(webinarsOnline!=null) {
			status = HttpStatus.OK;
			responseData.setView(ResponseView.WEBINARS_ONLINE);
			responseData.addElement(ResponseDataElementKeys.WEBINARS_ONLINE, webinarsOnline);
		}else {
			String errorMsg = "Error while trying to get webinars online";
			logger.error(errorMsg);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			responseErrors.add(new ResponseError(ResponseErrorEnum.CAN_NOT_FIND_RESPONSE_ELEMENTS, errorMsg));			
		}
		
		
		responseData.setErrors(responseErrors);
		return new ResponseEntity<>(responseData, status);
	}
	
	@RequestMapping(value=Api.GET_WEBINARS_OFFLINE)
	public ResponseEntity<ResponseData> getWebinarsOffline() {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();			
		List<ResponseError> responseErrors = new ArrayList<>();
		ResponseElement webinarsOffline = this.webinarService.getWebinarsHistoryAsResponseElement();
		if(webinarsOffline!=null) {
			status = HttpStatus.OK;
			responseData.setView(ResponseView.WEBINARS_OFFLINE);
			responseData.addElement(ResponseDataElementKeys.WEBINARS_OFFLINE, webinarsOffline);
		}else {
			String errorMsg = "Error while trying to get webinars offline";
			logger.error(errorMsg);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			responseErrors.add(new ResponseError(ResponseErrorEnum.CAN_NOT_FIND_RESPONSE_ELEMENTS, errorMsg));			
		}
		
		
		responseData.setErrors(responseErrors);
		return new ResponseEntity<>(responseData, status);
		
	}
	
	@RequestMapping(value=Api.CREATE_NEW_WEBINAR, method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<ResponseData> createNewWebinar(@RequestBody JsonNewWebinar newWebinarData, HttpServletRequest request) {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();			
		List<ResponseError> responseErrors = new ArrayList<>();
		Long webinarCreatorId = JwtTokenUtil.getUserIdFromToken(Utils.getTokenFromRequest(request), this.userService);		
		Webinar newWebinar = this.webinarService.createAndReturnNewWebinar(webinarCreatorId, newWebinarData.getTitle(), newWebinarData.getDescription(), newWebinarData.isAutostart());
		if(newWebinar != null) {
			status = HttpStatus.OK;	
			responseData.setView(ResponseView.WEBINAR);
			responseData.addElement(ResponseDataElementKeys.WEBINAR_OBJECT, new ResponseElement(ElementType.WEBINAR_OBJECT, newWebinar));
			
		}else {
			String errorMsg = "Webinar creation failed.";
			logger.error(errorMsg);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			responseErrors.add(new ResponseError(ResponseErrorEnum.OBJECT_NOT_CREATED, errorMsg));		
		}
		responseData.setErrors(responseErrors);
		return new ResponseEntity<>(responseData, status);
	}
	
	@RequestMapping(value=Api.START_EXISTING_WEBINAR, method=RequestMethod.GET)
	public ResponseEntity<ResponseData> startExistingWebinar() {
		return null;
	}
	
	@RequestMapping(value=Api.JOIN_WEBINAR, method=RequestMethod.GET)
	public ResponseEntity<ResponseData> joinWebinar(@PathVariable("webinarId") long webinarId, HttpServletRequest request) {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();			
		List<ResponseError> responseErrors = new ArrayList<>();
		String username = JwtTokenUtil.getUsernameFromToken(Utils.getTokenFromRequest(request));
		Webinar webinar = this.webinarService.joinWebinar(username, webinarId);
		if(webinar != null) {
			status = HttpStatus.OK;	
			responseData.setView(ResponseView.WEBINAR);
			responseData.addElement(ResponseDataElementKeys.WEBINAR_OBJECT, new ResponseElement(ElementType.WEBINAR_OBJECT, webinar));
			
		}else {
			String errorMsg = "Webinar joining failed.";
			logger.error(errorMsg);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			responseErrors.add(new ResponseError(ResponseErrorEnum.INTERNAL_ERROR, errorMsg));		
		}
		responseData.setErrors(responseErrors);
		return new ResponseEntity<>(responseData, status);
	}

}

package emesch.edu.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import emesch.edu.Api;
import emesch.edu.Utils;
import emesch.edu.data.ElementType;
import emesch.edu.data.ResponseData;
import emesch.edu.data.ResponseDataElementKeys;
import emesch.edu.data.ResponseElement;
import emesch.edu.data.ResponseView;
import emesch.edu.data.error.ResponseError;
import emesch.edu.data.error.ResponseErrorEnum;
import emesch.edu.model.course.Course;
import emesch.edu.model.json.JsonNewCourse;
import emesch.edu.model.json.JsonNewLesson;
import emesch.edu.security.JwtTokenUtil;
import emesch.edu.services.CourseService;
import emesch.edu.services.UserService;
import emesch.edu.utils.ControllerUtils;

@RestController
public class CourseRestController {
	
	public static Logger logger = Logger.getLogger(CourseRestController.class.getName());
	
	private CourseService courseService;
	private UserService userService;
	
	@Value("${emesch.config.QUERY_LIMIT}") 
	private int queryLimit;
	
	@Value("${emesch.config.POPULAR_QUERY_LIMIT}") 
	private int popularQueryLimit;
	
	@Autowired
	public CourseRestController(CourseService courseService, UserService userService) {
		super();
		this.courseService = courseService;
		this.userService = userService;
	}	

	@RequestMapping(value = Api.GET_ALL_COURSES, method = RequestMethod.GET)
	public ResponseEntity<ResponseData> getAllCourses() {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();
		Map<String, ResponseElement> allCourseElements = this.courseService.findAllCourseElements(queryLimit);
		List<ResponseError> responseErrors = new ArrayList<>();
		if(allCourseElements != null) {
			status = HttpStatus.OK;	
			responseData.setView(ResponseView.ALL_COURSES);
			responseData.setElements(allCourseElements);			
		}else {
			status = HttpStatus.NOT_FOUND;
			responseErrors.add(new ResponseError(ResponseErrorEnum.CAN_NOT_FIND_RESPONSE_ELEMENTS, "allCourseElements == null"));			
		}
		responseData.setErrors(responseErrors);
		return new ResponseEntity<>(responseData, status);
	}
	
	@RequestMapping(value = Api.GET_MY_COURSES, method = RequestMethod.GET)
	public ResponseEntity<ResponseData> getMyCourses(HttpServletRequest request) {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();
		Long userId = JwtTokenUtil.getUserIdFromToken(Utils.getTokenFromRequest(request), this.userService);
		Map<String, ResponseElement> myCourseElements = this.courseService.findUserCourseElements(userId, queryLimit);
		List<ResponseError> responseErrors = new ArrayList<>();
		if(myCourseElements != null) {
			status = HttpStatus.OK;	
			responseData.setView(ResponseView.MY_COURSES);
			responseData.setElements(myCourseElements);			
		}else {
			status = HttpStatus.NOT_FOUND;
			responseErrors.add(new ResponseError(ResponseErrorEnum.CAN_NOT_FIND_RESPONSE_ELEMENTS, "allCourseElements == null"));			
		}
		responseData.setErrors(responseErrors);
		return new ResponseEntity<>(responseData, status);
	}
	
	@RequestMapping(value = Api.GET_POPULAR_COURSES, method = RequestMethod.GET)
	public ResponseEntity<ResponseData> getPopularCourses() {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();
		Map<String, ResponseElement> popularCourseElements = this.courseService.findPopularCourseElements(popularQueryLimit);
		List<ResponseError> responseErrors = new ArrayList<>();
		if(popularCourseElements != null) {
			status = HttpStatus.OK;	
			responseData.setView(ResponseView.POPULAR_COURSES);
			responseData.setElements(popularCourseElements);			
		}else {
			status = HttpStatus.NOT_FOUND;
			responseErrors.add(new ResponseError(ResponseErrorEnum.CAN_NOT_FIND_RESPONSE_ELEMENTS, "popularCourseElements == null"));			
		}
		responseData.setErrors(responseErrors);
		return new ResponseEntity<>(responseData, status);
	}
	
	@RequestMapping(value = Api.CREATE_NEW_COURSE, method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<ResponseData> createCourse(@RequestBody JsonNewCourse newCourseData, HttpServletRequest request) {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();
		List<ResponseError> responseErrors = new ArrayList<>();
		Long courseCreatorId = JwtTokenUtil.getUserIdFromToken(Utils.getTokenFromRequest(request), this.userService);
		long newCourseId = this.courseService.createNewCourseAndReturnCourseId(courseCreatorId, newCourseData.getTitle(), newCourseData.getDescription(), newCourseData.getScope());		
		if(newCourseId>0) {
			status = HttpStatus.CREATED;
			String newCourseUrl = Api.GET_ALL_COURSES + "/" + newCourseId;
			responseData.addElement(ResponseDataElementKeys.NEW_COURSE_URL, new ResponseElement(ElementType.API_URL, newCourseUrl));
			logger.info("New course created! Url: "+newCourseUrl);
		} else {
			String errorMsg = "Course creation failed.";
			logger.error(errorMsg);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			responseErrors.add(new ResponseError(ResponseErrorEnum.OBJECT_NOT_CREATED, errorMsg));
		}	
		
		responseData.setErrors(responseErrors);
		return new ResponseEntity<>(responseData, status);
	}	
 	
	@RequestMapping(value = Api.GET_ALL_COURSES+"/{courseId}", method = RequestMethod.GET)
	public ResponseEntity<ResponseData> getCourseView(@PathVariable("courseId") long courseId) {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();
		List<ResponseError> responseErrors = new ArrayList<>();
				
		Course courseObject = this.courseService.findCourseById(courseId);		
		
		if(courseObject!=null) {
			status = HttpStatus.OK;	
			responseData.setView(ResponseView.COURSE);
			responseData.addElement(ResponseDataElementKeys.COURSE_OBJECT, new ResponseElement(ElementType.COURSE_OBJECT, courseObject));			
		} else {
			status = HttpStatus.NOT_FOUND;
			responseErrors.add(new ResponseError(ResponseErrorEnum.CAN_NOT_FIND_OBJECT, "courseObject==null"));
		}		
		
		return new ResponseEntity<>(responseData, status);
	}	

}

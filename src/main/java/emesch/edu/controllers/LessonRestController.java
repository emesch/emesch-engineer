package emesch.edu.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import emesch.edu.Api;
import emesch.edu.Utils;
import emesch.edu.data.ElementType;
import emesch.edu.data.ResponseData;
import emesch.edu.data.ResponseDataElementKeys;
import emesch.edu.data.ResponseElement;
import emesch.edu.data.ResponseView;
import emesch.edu.data.error.ResponseError;
import emesch.edu.data.error.ResponseErrorEnum;
import emesch.edu.model.course.Course;
import emesch.edu.model.course.Lesson;
import emesch.edu.model.json.JsonNewLesson;
import emesch.edu.security.JwtTokenUtil;
import emesch.edu.services.CourseService;
import emesch.edu.services.LessonService;
import emesch.edu.services.UserService;
import emesch.edu.utils.ControllerUtils;

@RestController
public class LessonRestController {
	
	public static Logger logger = Logger.getLogger(LessonRestController.class.getName());
	
	private CourseService courseService;
	private LessonService lessonService;
	private UserService userService;
	
	@Value("${emesch.config.QUERY_LIMIT}") 
	private int queryLimit;
	
	@Value("${emesch.config.POPULAR_QUERY_LIMIT}") 
	private int popularQueryLimit;
	
	@Autowired
	public LessonRestController(CourseService courseService, LessonService lessonService, UserService userService) {
		super();
		this.courseService = courseService;
		this.lessonService = lessonService;
		this.userService = userService;
	}
	
	@RequestMapping(value = Api.CREATE_NEW_COURSE_LESSON, method = RequestMethod.POST, consumes = "application/json")
	public ResponseEntity<ResponseData> createLesson(@RequestBody @Valid JsonNewLesson newLessonData, BindingResult bindingResult, HttpServletRequest request) {
		if(bindingResult.hasErrors()) {
			return ControllerUtils.validationError("Lesson creation failed", bindingResult);
		}else {
			Long lessonCreatorId = JwtTokenUtil.getUserIdFromToken(Utils.getTokenFromRequest(request), this.userService);
			long courseId = newLessonData.getCourseId();
			long newLessonId = this.courseService.createNewLessonAndReturnLessonId(lessonCreatorId, newLessonData);	
			String newLessonUrl = Api.GET_ALL_COURSES + "/" + courseId + "/" + newLessonId;
			if(newLessonId>0) {
				logger.debug("New lesson created! Url: "+newLessonUrl);					
				return ControllerUtils.creationSuccess(ResponseDataElementKeys.NEW_LESSON_URL, newLessonUrl);
				
			} else {
				return ControllerUtils.creationFailed("Lesson creation failed", logger);
			}	
		}	
	}
	
	@RequestMapping(value = Api.GET_ALL_COURSES+"/{courseId}/{lessonId}", method = RequestMethod.GET)
	public ResponseEntity<ResponseData> getLessonView(@PathVariable("courseId") long courseId, @PathVariable("lessonId") long lessonId) {
		HttpStatus status = null;
		ResponseData responseData = new ResponseData();
		List<ResponseError> responseErrors = new ArrayList<>();
				
		Course courseObject = this.courseService.findCourseById(courseId);
		Lesson lessonObject = this.lessonService.findLessonById(lessonId);
		
		if(courseObject!=null && lessonObject !=null) {
			status = HttpStatus.OK;	
			responseData.setView(ResponseView.LESSON);
			responseData.addElement(ResponseDataElementKeys.COURSE_OBJECT, new ResponseElement(ElementType.COURSE_OBJECT, courseObject));
			responseData.addElement(ResponseDataElementKeys.LESSON_OBJECT, new ResponseElement(ElementType.LESSON_OBJECT, lessonObject));
		} else {
			status = HttpStatus.NOT_FOUND;
			responseErrors.add(new ResponseError(ResponseErrorEnum.CAN_NOT_FIND_OBJECT, "courseObject or lessonObject"));
		}		
		
		return new ResponseEntity<>(responseData, status);
	}

}

package emesch.edu.controllers;

import org.apache.log4j.Logger;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

@Controller
public class TestMessageController {
	
	public static Logger logger = Logger.getLogger(TestMessageController.class.getName());
	
	@MessageMapping("/testwebinarium")
	public void handleTestWeb() {
		logger.info("Odebrano komunikat: " );
	}

}

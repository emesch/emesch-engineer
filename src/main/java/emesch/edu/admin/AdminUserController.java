package emesch.edu.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import emesch.edu.Api;
import emesch.edu.model.users.User;
import emesch.edu.repositories.UserRepository;

@RestController
public class AdminUserController {

	private UserRepository userRepository;

	@Autowired
	public AdminUserController(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@RequestMapping(value = Api.ADMIN_GET_REGISTERED_USERS, method = RequestMethod.GET)
	public List<User> getRegisteredUsers() {		
		return this.userRepository.findAllUsers();		
	}

}



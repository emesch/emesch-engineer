package emesch.edu;

import javax.servlet.http.HttpServletRequest;

import emesch.edu.errors.InvalidTokenException;
import emesch.edu.security.controllers.AuthenticationRestController;

public class Utils {	
	
	public static String getTokenFromRequest(HttpServletRequest request) {
		String authHeader = request.getHeader(getTokenHeader());
		String token = null;
		if(authHeader.startsWith("Bearer ")) {
			token = authHeader.substring(7);
		}else {
			throw new InvalidTokenException(InvalidTokenException.BEARER_ABSENCE);
		}

		return token;
	}
	
	public static String getTokenHeader() {
		return AuthenticationRestController.tokenHeader;
	}

}

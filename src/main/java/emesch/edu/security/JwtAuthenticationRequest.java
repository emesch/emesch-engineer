package emesch.edu.security;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

import emesch.edu.repositories.UserRepository;

public class JwtAuthenticationRequest implements Serializable {	

	private static final long serialVersionUID = -3856447976839509335L;
	
	private UserRepository userRepository;

	@JsonProperty
	private String username;
	@JsonProperty
	private String email;
	@JsonProperty
	private String password;    

	
	public JwtAuthenticationRequest() {
		
	}
	
	public JwtAuthenticationRequest(String username, String email, String password) {
		this.setUsername(username);
		this.setEmail(email);
		this.setPassword(password);
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}

package emesch.edu.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import emesch.edu.data.error.ResponseError;
import emesch.edu.data.error.ResponseErrorEnum;
import emesch.edu.errors.InvalidTokenException;

public class BadRequestFilter extends OncePerRequestFilter {

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		try {
			filterChain.doFilter(request, response);
		} catch (InvalidTokenException e) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
			response.getWriter().write(ResponseError.getJsonError(ResponseErrorEnum.INVALID_TOKEN, e.getMessage()));
		}
	}	

}



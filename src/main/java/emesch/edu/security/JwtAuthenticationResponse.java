package emesch.edu.security;

import java.io.Serializable;

public class JwtAuthenticationResponse implements Serializable { 
	
	private static final long serialVersionUID = -7829440393489120280L;
	
	private final String token;	

    public JwtAuthenticationResponse(String token) {
        this.token = token;
    }

    public String getToken() {
        return this.token;
    }
}
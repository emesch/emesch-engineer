package emesch.edu.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import emesch.edu.data.ElementType;
import emesch.edu.data.ResponseElement;
import emesch.edu.model.users.AuthorityName;
import emesch.edu.model.users.JwtUser;
import emesch.edu.model.users.User;
import emesch.edu.model.users.UserProfile;
import emesch.edu.model.users.UserProfileElement;
import emesch.edu.model.users.UserProfileElementName;
import emesch.edu.repositories.UserRepository;

@Service
public class UserService {
	
	private UserRepository userRepository;
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}
	
	public User registerUser(String username, String email, String password) {
		String encryptedPassword = passwordEncoder.encode(password);			
		this.userRepository.addUserToDatabase(username, email, true, getDefaultAuthorities(), encryptedPassword, getDefaultProfileElements(email));
		return this.userRepository.findUserByEmail(email);
	}

	public UserProfile getPublicUserProfile(long profileId) {
		return this.userRepository.getPublicUserProfile(profileId);
	}

	public Map<String, ResponseElement> getAllInfoAboutFriendsOfUser(long userId) {
		return this.userRepository.getAllInfoAboutFriendsOfUser(userId);
	}
	
	public static List<AuthorityName> getDefaultAuthorities() {
		List<AuthorityName> authoritiesList = new ArrayList<>();
		authoritiesList.add(AuthorityName.ROLE_USER);
		return authoritiesList;
	}

	public static List<UserProfileElement> getDefaultProfileElements(String email) {
		List<UserProfileElement> profileElements = new ArrayList<>();
		profileElements.add(new UserProfileElement(UserProfileElementName.EMAIL, ElementType.EMAIL, email, true));
		return profileElements;
	}

	public User findUserByUsername(String username) {
		return this.userRepository.findUserByUsername(username);
	}	

}

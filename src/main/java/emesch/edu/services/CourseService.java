package emesch.edu.services;

import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import emesch.edu.data.ResponseElement;
import emesch.edu.model.course.Course;
import emesch.edu.model.json.JsonNewLesson;
import emesch.edu.repositories.CourseRepository;
import emesch.edu.repositories.LessonRepository;

@Service
public class CourseService {
	
	private CourseRepository courseRepository;
	private LessonRepository lessonRepository;
	
	@Autowired
	public CourseService(CourseRepository courseRepository, LessonRepository lessonRepository) {
		this.courseRepository = courseRepository;
		this.lessonRepository = lessonRepository;
	}

	@Transactional
	public long createNewCourseAndReturnCourseId(Long courseCreatorId, String title, String description, String scope) {		
		KeyHolder newCourseKeyHolder = courseRepository.createNewCourse(courseCreatorId, title, description, scope);				
		
		addCourseCreatorToCourse(courseCreatorId, newCourseKeyHolder);		
		
		addDefaultChapterToCourse(newCourseKeyHolder);
		
		//TODO: course elements creation
		//String sqlInsertNewCourseDetails
		//this.jdbcTemplate.update(sqlInsertNewCourseDetails, )
		
		return newCourseKeyHolder.getKey().longValue();
	}

	public Long createNewLessonAndReturnLessonId(String title, String description) {
		return this.lessonRepository.createNewLessonAndReturnLessonId(title, description);
	}
	
	public Long createNewLessonAndReturnLessonId(long lessonCreatorId, JsonNewLesson newLessonData) {		
		Long lessonId = createNewLessonAndReturnLessonId(newLessonData.getTitle(), newLessonData.getDescription());		
		//TODO: lesson to chapter?? 
		addLessonToChapter(newLessonData.getChapterId(),  lessonId);
		return lessonId;
	}
	
	public void addLessonToChapter(long chapterId, long lessonId) {
		this.lessonRepository.addLessonToChapter(chapterId, lessonId);
	}
	
	public void addLessonToDefaultChapter(long courseId, Long lessonId) {		
		long chapterId = this.lessonRepository.findFirstChapterOfCourse(courseId).getId();
		addLessonToChapter(chapterId, lessonId);
	}
	
	public Map<String, ResponseElement> findAllCourseElements(int queryLimit) {
		return this.courseRepository.findAllCourseElements(queryLimit);	
	}
	
	public Map<String, ResponseElement> findUserCourseElements(Long userId, int queryLimit) {
		return null;//return this.courseRepository.findUserCourseElements(userId, queryLimit);
	}
	
	public Map<String, ResponseElement> findPopularCourseElements(int queryLimit) {
		return this.courseRepository.findPopularCourseElements(queryLimit);
	}

	public Course findCourseById(long courseId) {
		return getFullCourseObject(courseId);
	}

	public String getDefaultScope() {
		return "PUBLIC";
	}

	private void addCourseCreatorToCourse(Long courseCreatorId, KeyHolder newCourseKeyHolder) {
		courseRepository.addInstructorToCourse(courseCreatorId, newCourseKeyHolder);
	}

	private void addDefaultChapterToCourse(KeyHolder newCourseKeyHolder) {
		KeyHolder newChapterKeyHolder = lessonRepository.createNewChapter("Default chapter");		
		lessonRepository.addChapterToCourse(newCourseKeyHolder, newChapterKeyHolder);
	}

	private Course getFullCourseObject(long courseId) {
		Course returnedCourse = courseRepository.findCourseById(courseId);
		returnedCourse.addChapters(lessonRepository.findAllChaptersOfCourse(courseId));
		return returnedCourse;
	}
	

}

package emesch.edu.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import emesch.edu.data.ElementType;
import emesch.edu.data.ResponseElement;
import emesch.edu.model.users.User;
import emesch.edu.model.webinar.Webinar;
import emesch.edu.repositories.WebinarRepository;

@Service
public class WebinarService {
	
	@Value("${emesch.config.POPULAR_QUERY_LIMIT}") 
	private int popularQueryLimit;
	
	private UserService userService;
	private WebinarRepository webinarRepository;
	
	private Map<Long, Webinar> webinarsOnline;

	@Autowired
	public WebinarService(UserService userService, WebinarRepository webinarRepository) {
		this.userService = userService;
		this.webinarRepository = webinarRepository;
		this.webinarsOnline = new HashMap<>();
	}
	
	public ResponseElement getWebinarsOnlineAsResponseElement() {
		return new ResponseElement(ElementType.LIST_OBJECT, webinarsOnline);
	}
	
	public ResponseElement getWebinarsHistoryAsResponseElement() {
		List<Webinar> webinarsOffline = this.webinarRepository.getWebinarsHistory(popularQueryLimit);				
		return new ResponseElement(ElementType.LIST_OBJECT, webinarsOffline);
	}

	public Webinar createAndReturnNewWebinar(Long webinarCreatorId, String title, String description, boolean autostart) {
		Webinar newWebinar = this.webinarRepository.createAndReturnNewWebinar(webinarCreatorId, title, description, autostart);
		if(isWebinarStarted(newWebinar)) {
			webinarsOnline.put(newWebinar.getId(), newWebinar);
		}
		return newWebinar;
	}
	
	private boolean isWebinarStarted(Webinar webinar) {
		return webinar.getStartTime()!=null && webinar.getEndTime()==null;
	}

	public Map<Long, Webinar> getWebinarsOnline() {
		return webinarsOnline;
	}

	public void startScheduledWebinars() {
		this.getScheduledWebinarsToStart().forEach(webinar -> {
			startWebinar(webinar);			
		});		
	}

	private void startWebinar(Webinar webinar) {
		Webinar updatedWebinar = this.webinarRepository.startWebinarAndReturnUpdated(webinar);
		webinarsOnline.put(updatedWebinar.getId(), updatedWebinar);		
	}

	private List<Webinar> getScheduledWebinarsToStart() {
		return this.webinarRepository.getScheduledWebinarsToStart();
	}

	public Webinar joinWebinar(String username, long webinarId) {
	    User user = this.userService.findUserByUsername(username);
		Webinar webinar = webinarsOnline.get(webinarId);
		webinar.joinUser(user);
		return webinar;
	}	

}

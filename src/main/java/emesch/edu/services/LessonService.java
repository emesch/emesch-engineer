package emesch.edu.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import emesch.edu.model.course.Lesson;
import emesch.edu.repositories.LessonRepository;

@Service
public class LessonService {
	
	private LessonRepository lessonRepository;
	
	@Autowired
	public LessonService(LessonRepository lessonRepository) {
		this.lessonRepository = lessonRepository;
	}

	public Lesson findLessonById(long lessonId) {
		return this.lessonRepository.findLessonById(lessonId);		
	}	

}

package emesch.edu.repositories;

import java.util.List;
import java.util.Map;

import emesch.edu.data.ResponseElement;
import emesch.edu.model.users.AuthorityName;
import emesch.edu.model.users.User;
import emesch.edu.model.users.UserProfile;
import emesch.edu.model.users.UserProfileElement;

public interface UserRepository {
	
	List<User> findAllUsers();
	
	User findUserById(long userId);
	
	User findUserByEmail(String email);
	
	User findUserByUsername(String username);
	
	UserProfile getPublicUserProfile(long userId);
	
	UserProfile getPrivateUserProfile(long userId);
	
	List<UserProfileElement> findPublicUserProfileElements(long userId);
	
	List<UserProfileElement> findInstructorProfileElements(long instructorId);
	
	void addUserToDatabase(String username, String email, boolean enabled, List<AuthorityName> authoritiesList, String password, List<UserProfileElement> profileElements);

	Map<String, ResponseElement> getAllInfoAboutFriendsOfUser(long userId);
	
	List<ResponseElement> findAllFriendsIdsOfUser(long userId);	

}

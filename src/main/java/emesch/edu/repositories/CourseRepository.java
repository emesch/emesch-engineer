package emesch.edu.repositories;

import java.util.List;
import java.util.Map;

import org.springframework.jdbc.support.KeyHolder;

import emesch.edu.data.ResponseElement;
import emesch.edu.model.course.Course;
import emesch.edu.model.users.UserProfile;

public interface CourseRepository {

	KeyHolder createNewCourse(Long courseCreatorId, String title, String desc, String scope);
	void addInstructorToCourse(Long courseCreatorId, KeyHolder newCourseKeyHolder);	
	Course findCourseById(long courseId);
	Map<String, ResponseElement> findAllCourseElements(int queryLimit);
	Map<String, ResponseElement> findUserCourseElements(Long userId, int queryLimit);
	Map<String, ResponseElement> findPopularCourseElements(int queryLimit);	
	List<Course> findAllCourses(int queryLimit);	
	List<UserProfile> findAllCourseInstructorProfiles(long courseId);	
}

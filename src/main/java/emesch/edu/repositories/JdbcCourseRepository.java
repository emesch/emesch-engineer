package emesch.edu.repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import emesch.edu.data.ElementType;
import emesch.edu.data.ResponseDataElementKeys;
import emesch.edu.data.ResponseElement;
import emesch.edu.model.course.Course;
import emesch.edu.model.course.CourseAccess;
import emesch.edu.model.users.UserProfile;
import emesch.edu.model.users.UserProfileElement;
import emesch.edu.utils.TableNames;

@Repository
public class JdbcCourseRepository implements CourseRepository {	
	
	private JdbcTemplate jdbcTemplate;
	private UserRepository userRepository;	
	
	private final String SQL_FIND_ALL_COURSES = "select id, title, course_creator_id, rating, votes from "+TableNames.COURSES+" limit ?";
	private final String SQL_FIND_ALL_COURSE_ELEMENTS = "select id, title, course_creator_id, rating, votes from "+TableNames.COURSES+" limit ?";
	
	private final String SQL_FIND_POPULAR_COURSE_ELEMENTS = "select id, title, course_creator_id, rating, votes from "+TableNames.COURSES+" limit ?";
	
	private final String SQL_FIND_USER_COURSE_ELEMENTS = "select id, title, course_creator_id, rating, votes from "+TableNames.COURSES+" where course_creator_id = ? limit ?";
	private final String SQL_FIND_COURSE_BY_ID = "select id, title, course_creator_id, rating, votes from "+TableNames.COURSES+" where id = ?";
	
	private final String SQL_FIND_ALL_COURSE_INSTRUCTORS_PROFILES = "select instructor_id from "+TableNames.COURSES_INSTRUCTORS+" where course_id = ?";	
	
	private final String SQL_INSERT_NEW_COURSE = "insert into "+TableNames.COURSES+"(course_creator_id, title) values (?, ?)";
	private final String SQL_INSERT_NEW_COURSE_INSTRUCTOR = "insert into "+TableNames.COURSES_INSTRUCTORS+"(course_id, instructor_id) values (?, ?)";		
	
	@Autowired
	public JdbcCourseRepository(JdbcTemplate jdbcTemplate, UserRepository userRepository) {
		this.jdbcTemplate = jdbcTemplate;
		this.userRepository = userRepository;		
	}
	
	@Override	
	public KeyHolder createNewCourse(Long courseCreatorId, String title, String desc, String scope){		
		KeyHolder newCourseKeyHolder = new GeneratedKeyHolder();
		
		addNewCourse(courseCreatorId, title, newCourseKeyHolder);	
		
		if(newCourseKeyHolder.getKey()==null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		return newCourseKeyHolder;
	}	

	@Override
	public void addInstructorToCourse(Long courseInstructorId, KeyHolder newCourseKeyHolder) {
		this.jdbcTemplate.update(SQL_INSERT_NEW_COURSE_INSTRUCTOR, newCourseKeyHolder.getKey(), courseInstructorId);
	}

	@Override
	public List<Course> findAllCourses(int queryLimit) {
		return this.jdbcTemplate.query(SQL_FIND_ALL_COURSES, new Object[] {queryLimit}, this::mapCourse);
	}

	@Override
	public Map<String, ResponseElement> findAllCourseElements(int queryLimit) {
		Map<String, ResponseElement> returnedMap = new HashMap<>();
		List<ResponseElement> allCourseElements = this.jdbcTemplate.query(SQL_FIND_ALL_COURSE_ELEMENTS, new Object[] {queryLimit}, this::mapCourseElement);		
		
		returnedMap.put(ResponseDataElementKeys.COURSES_LIST, new ResponseElement(ElementType.LIST_OBJECT, allCourseElements));
		
		return returnedMap;
	}
	
	@Override
	public Map<String, ResponseElement> findPopularCourseElements(int queryLimit) {
		Map<String, ResponseElement> returnedMap = new HashMap<>();
		//TODO: Need to change in future - not all courses but popular!
		List<ResponseElement> allCourseElements = this.jdbcTemplate.query(SQL_FIND_POPULAR_COURSE_ELEMENTS, new Object[] {queryLimit}, this::mapCourseElement);		
		
		returnedMap.put(ResponseDataElementKeys.COURSES_LIST, new ResponseElement(ElementType.LIST_OBJECT, allCourseElements));
		
		return returnedMap;
	}
	
	@Override
	public Map<String, ResponseElement> findUserCourseElements(Long userId, int queryLimit) {
		Map<String, ResponseElement> returnedMap = new HashMap<>();
		List<ResponseElement> userCourseElements = this.jdbcTemplate.query(SQL_FIND_USER_COURSE_ELEMENTS, new Object[] {userId, queryLimit}, this::mapCourseElement);		
		
		returnedMap.put(ResponseDataElementKeys.COURSES_LIST, new ResponseElement(ElementType.LIST_OBJECT, userCourseElements));
		
		return returnedMap;
	}
	
	@Override
	public Course findCourseById(long courseId) {		
		return this.jdbcTemplate.queryForObject(SQL_FIND_COURSE_BY_ID, new Object[]{courseId}, this::mapCourse);		 
	}	
	
	@Override
	public List<UserProfile> findAllCourseInstructorProfiles(long courseId) {
		List<UserProfile> instructorProfiles = new ArrayList<>();
		List<Long> instructorIds = this.jdbcTemplate.query(SQL_FIND_ALL_COURSE_INSTRUCTORS_PROFILES, new Object[] {courseId}, this::mapInstructorId);		
		
		for(long instructorId : instructorIds) {
			List<UserProfileElement> instructorElements = this.userRepository.findInstructorProfileElements(instructorId);
			instructorProfiles.add(new UserProfile(instructorId, instructorElements));
		}	
		
		return instructorProfiles;		
	}	
	
	private void addNewCourse(Long courseCreatorId, String title, KeyHolder newCourseKeyHolder) {
		this.jdbcTemplate.update((connection) -> {					
			PreparedStatement ps = connection.prepareStatement(SQL_INSERT_NEW_COURSE);			
			ps.setLong(1, courseCreatorId);
			ps.setString(2, title);
			return ps;			
		} , newCourseKeyHolder);
	}		
	
	private Long mapInstructorId(ResultSet rs, int rowNum) throws SQLException {
		return rs.getLong("instructor_id");
	}
	
	private Course mapCourse(ResultSet rs, int rowNum) throws SQLException {
		//TODO: setting CourseAccess for New Course is hardcoded - need change in future
		Course returnedCourse = new Course(rs.getLong("id"), rs.getString("title"), rs.getLong("course_creator_id"), CourseAccess.PUBLIC, rs.getLong("rating"), rs.getLong("votes"));
		returnedCourse.addInstructors(findAllCourseInstructorProfiles(rs.getLong("id")));		
		return returnedCourse;		
	}	

	private ResponseElement mapCourseElement(ResultSet rs, int rowNum) throws SQLException {		
		//TODO: setting CourseAccess for New Course is hardcoded - need change in future
		Course courseObject = mapCourse(rs, rowNum);		
		return new ResponseElement(ElementType.COURSE_OBJECT, courseObject);
	}

}

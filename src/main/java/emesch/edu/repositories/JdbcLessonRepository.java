package emesch.edu.repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import emesch.edu.model.course.Chapter;
import emesch.edu.model.course.Lesson;
import emesch.edu.utils.TableNames;

@Repository
public class JdbcLessonRepository implements LessonRepository {	
	
	private final String SQL_ADD_CHAPTER_TO_COURSE = "insert into "+TableNames.COURSES_CHAPTERS+" (course_id, chapter_id) values (?, ?)";
	private final String SQL_ADD_LESSON_TO_CHAPTER =  "insert into "+TableNames.CHAPTERS_LESSONS+"(chapter_id, lesson_id) values (?, ?)";	
	private final String SQL_INSERT_NEW_LESSON = "insert into "+TableNames.LESSONS+"(title, description) values (?, ?)";
	private final String SQL_INSERT_NEW_CHAPTER = "insert into "+TableNames.CHAPTERS+" (title) values (?)";
	private final String SQL_FIND_ALL_COURSE_CHAPTERS = "select * from "+TableNames.CHAPTERS+" where id in (select chapter_id from courses_chapters where course_id = ?)";
	private final String SQL_FIND_COURSE_FIRST_CHAPTER = "select * from "+TableNames.CHAPTERS+" where id in (select chapter_id from courses_chapters where course_id = ?) limit 1";
	private final String SQL_FIND_ALL_CHAPTER_LESSONS = "select * from "+TableNames.LESSONS+" where id in (select lesson_id from "+TableNames.CHAPTERS_LESSONS+" where chapter_id = ?)";
	private final String SQL_FIND_LESSON_BY_ID = "select * from "+TableNames.LESSONS+" where id = ?";
	
	private JdbcTemplate jdbcTemplate;	
	
	@Autowired
	public JdbcLessonRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;		
	}
	
	@Override
	public void addChapterToCourse(KeyHolder newCourseKeyHolder, KeyHolder newChapterKeyHolder) {
		this.jdbcTemplate.update(SQL_ADD_CHAPTER_TO_COURSE, newCourseKeyHolder.getKey(), newChapterKeyHolder.getKey());
	}

	@Override
	public void addLessonToChapter(long chapterId, long lessonId) {
		this.jdbcTemplate.update(SQL_ADD_LESSON_TO_CHAPTER, chapterId, lessonId);
	}	

	@Override
	public long createNewLessonAndReturnLessonId(String courseTitle, String courseDescription) {
		KeyHolder newLessonKeyHolder = new GeneratedKeyHolder();
		
		this.jdbcTemplate.update((connection) -> {
			PreparedStatement ps = connection.prepareStatement(SQL_INSERT_NEW_LESSON);
			ps.setString(1, courseTitle);
			ps.setString(2, courseDescription);
			return ps;
		}, newLessonKeyHolder);
		
		if(newLessonKeyHolder.getKey()==null) return -1;
		
		return newLessonKeyHolder.getKey().longValue();		
	}
	
	@Override
	public KeyHolder createNewChapter(String chapterTitle) {		
		KeyHolder newChapterKeyHolder = new GeneratedKeyHolder();
		
		this.jdbcTemplate.update((connection) -> {
			PreparedStatement ps = connection.prepareStatement(SQL_INSERT_NEW_CHAPTER);
			ps.setString(1, chapterTitle);
			return ps;
		}, newChapterKeyHolder);
		
		return newChapterKeyHolder;
	}	
		
	@Override
	public List<Chapter> findAllChaptersOfCourse(long courseId) {
		return this.jdbcTemplate.query(SQL_FIND_ALL_COURSE_CHAPTERS, new Object[] {courseId}, this::mapChapter);
	}
	
	@Override
	public Chapter findFirstChapterOfCourse(long courseId) {
       return this.jdbcTemplate.queryForObject(SQL_FIND_COURSE_FIRST_CHAPTER, new Object[] {courseId}, this::mapChapter);
	}


	@Override
	public List<Lesson> findAllChapterLessons(long chapterId) {
		return this.jdbcTemplate.query(SQL_FIND_ALL_CHAPTER_LESSONS, new Object[] {chapterId}, this::mapLesson);
	}

	@Override
	public Lesson findLessonById(long lessonId) {
		return this.jdbcTemplate.queryForObject(SQL_FIND_LESSON_BY_ID, new Object[] {lessonId}, this::mapLesson);
	}

	private Chapter mapChapter(ResultSet rs, int rowNum) throws SQLException {
		List<Lesson> lessons = findAllChapterLessons(rs.getLong("id"));
		return new Chapter(rs.getLong("id"), rs.getString("title"), lessons);
	}
	
	private Lesson mapLesson(ResultSet rs, int rowNum) throws SQLException {
		return new Lesson(rs.getLong("id"), rs.getString("title"), Duration.ZERO);
	}

}

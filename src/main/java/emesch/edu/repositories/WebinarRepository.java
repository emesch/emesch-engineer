package emesch.edu.repositories;

import java.util.List;
import java.util.Set;

import emesch.edu.model.webinar.Webinar;

public interface WebinarRepository {	
	
	Webinar findWebinarById(Long webinarId);
	List<Webinar> findWebinarsOnline(int popularQueryLimit);
	List<Webinar> getScheduledWebinarsToStart();
	Webinar createAndReturnNewWebinar(Long webinarCreatorId, String title, String description, boolean autostart);
	Webinar startWebinarAndReturnUpdated(Webinar webinar);
	List<Webinar> getWebinarsHistory(int popularQueryLimit);
	
}

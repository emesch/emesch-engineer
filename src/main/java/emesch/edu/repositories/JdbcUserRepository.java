package emesch.edu.repositories;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import emesch.edu.data.ElementType;
import emesch.edu.data.ResponseDataElementKeys;
import emesch.edu.data.ResponseElement;
import emesch.edu.model.users.Authority;
import emesch.edu.model.users.AuthorityName;
import emesch.edu.model.users.User;
import emesch.edu.model.users.UserProfile;
import emesch.edu.model.users.UserProfileElement;
import emesch.edu.model.users.UserProfileElementName;

@Repository
public class JdbcUserRepository implements UserRepository {
	
	public static Logger logger = Logger.getLogger(JdbcUserRepository.class.getName());
	
	private JdbcTemplate jdbcTemplate;
	
	private static final String FIND_PUBLIC_USER_PROFILE_ELEMENTS = 
			"select u.username, upe.element_name_id, upe.element_type_id, upe.element_value, upe.is_public "
			+ "from user_profile_elements as upe "
			+ "LEFT JOIN users as u on u.id=upe.user_id "
			+ "where upe.user_id = ? and upe.is_public = true";
	
	@Autowired
	public JdbcUserRepository(JdbcTemplate jdbcTemplate){
		this.jdbcTemplate = jdbcTemplate;
	}
	
	@Override
	public List<User> findAllUsers() {
		return this.jdbcTemplate.query("select id, username, email, enabled from users", this::mapUser);
	}
	
	@Override
	public User findUserById(long userId) {
		return this.jdbcTemplate.queryForObject("select id, username, email, enabled, password from users where id = ?", new Object[] {userId}, this::mapUser);
	}
	
	@Override
	public User findUserByEmail(String email) {		
		return (User)this.jdbcTemplate.queryForObject("select id, username, email, enabled, password from users where email = ?", new Object[] {email}, this::mapUser);
	};
	
	@Override
	public User findUserByUsername(String username) {		
		return (User)this.jdbcTemplate.queryForObject("select id, username, email, enabled, password from users where username = ?", new Object[] {username}, this::mapUser);
	}
	
	@Override
	public UserProfile getPublicUserProfile(long userId) {
		List<UserProfileElement> profileElements = findPublicUserProfileElements(userId);
		if(profileElements==null || profileElements.size()==0) {
			return null;
		}
		return new UserProfile(userId, profileElements);
	}
	
	@Override
	public UserProfile getPrivateUserProfile(long userId) {
		List<UserProfileElement> profileElements = findPrivateUserProfileElements(userId);
		return new UserProfile(userId, profileElements);		
	}	
	
	@Override
	public Map<String, ResponseElement> getAllInfoAboutFriendsOfUser(long userId) {
		Map<String, ResponseElement> responseElementsMap = new HashMap<>();
		List<ResponseElement> friendsIdsElements = findAllFriendsIdsOfUser(userId);
		
		responseElementsMap.put(ResponseDataElementKeys.USER_FRIENDS_COUNT, new ResponseElement(ElementType.NUMBER_LONG, countFriendsOfUser(userId)));
		responseElementsMap.put(ResponseDataElementKeys.USER_FRIENDS_ID_LIST, new ResponseElement(ElementType.LIST_IDS, friendsIdsElements));
		
		return responseElementsMap;		
	}
	
	@Override
	public List<ResponseElement> findAllFriendsIdsOfUser(long userId) {
		List<ResponseElement> allFriendsIds = new ArrayList<>();
		
		List<ResponseElement> friendsIds1 = this.jdbcTemplate.query("select inviting_user_id as friend_id from user_invitations where invited_user_id=? and is_accepted = true", new Object[] {userId}, this::mapFriendsIdsToResponseElements);
		List<ResponseElement> friendsIds2 = this.jdbcTemplate.query("select invited_user_id as friend_id from user_invitations where inviting_user_id=? and is_accepted = true", new Object[] {userId}, this::mapFriendsIdsToResponseElements);
		allFriendsIds.addAll(friendsIds1);
		allFriendsIds.addAll(friendsIds2);
		
		return allFriendsIds;
	}

	@Override
	public List<UserProfileElement> findPublicUserProfileElements(long userId) {
		List<UserProfileElement> publicUserProfileElements = new ArrayList<>();
		User user = findUserById(userId);
		publicUserProfileElements.add(new UserProfileElement(UserProfileElementName.USERNAME, ElementType.TEXT, user.getUsername(), true));
		publicUserProfileElements.addAll(this.jdbcTemplate.query(FIND_PUBLIC_USER_PROFILE_ELEMENTS, new Object[] {userId}, this::mapUserProfileElements));		
		return publicUserProfileElements;		
	}	

	@Transactional
	@Override
	public void addUserToDatabase(String username, String email, boolean enabled, List<AuthorityName> authoritiesList, String encryptedPassword, List<UserProfileElement> profileElements) {
		this.jdbcTemplate.update("insert into users (username, email, enabled, password) values (?, ?, ?, ?)", username, email, enabled, encryptedPassword);
		
		User createdUser = findUserByUsername(username);
		for(AuthorityName authorityName : authoritiesList) {
			addAuthorityForUserId(createdUser.getId(), authorityName);
		}
		for(UserProfileElement profileElement : profileElements){
			addProfileElementForUserId(createdUser.getId(), profileElement);
		}
		
	}
	
	@Override
	public List<UserProfileElement> findInstructorProfileElements(long instructorId) {
		List<UserProfileElement> instructorProfileElements = new ArrayList<>(); 
		for(UserProfileElement publicProfileElement : findPublicUserProfileElements(instructorId)) {
			if(publicProfileElement.getName().isInstructorProfileElement()) {
				instructorProfileElements.add(publicProfileElement);
			}		
		}
		return instructorProfileElements;		
	}		
	
	private void addProfileElementNameToDatabase(String name) {
		this.jdbcTemplate.update("insert into user_profile_element_names(element_name) values (?)", name);
	}
	
	private void addProfileElementTypeToDatabase(String elementType) {
		this.jdbcTemplate.update("insert into element_types(element_type) values (?)", elementType);		
	}

	private void addAuthorityToDatabase(AuthorityName authority) {
		logger.warn("Adding new authority to database: "+authority);
		this.jdbcTemplate.update("insert into authorities (name) values (?)", authority.name());		
	}

	private void addAuthorityForUserId(Long userId, AuthorityName authority) {
		Long authorityId = getAuthorityId(authority);
		this.jdbcTemplate.update("insert into user_authorities (user_id, authority_id) values (?, ?)", userId, authorityId);
	}
	
	private void addProfileElementForUserId(Long userId, UserProfileElement profileElement) {
		long elementNameId = getUserProfileElementNameId(profileElement);
		long elementTypeId = getUserProfileElementTypeId(profileElement);
		
		this.jdbcTemplate.update("insert into user_profile_elements (user_id, element_name_id, element_type_id, element_value, is_public) values (?, ?, ?, ?, ?)", userId, elementNameId, elementTypeId, profileElement.getValue(), profileElement.isPublicValue());		
	}
	
	private long countFriendsOfUser(long userId) {		
		return this.jdbcTemplate.queryForObject("select count(*) from user_invitations where inviting_user_id=? or invited_user_id=? and is_accepted=true", new Object[]{userId, userId}, Long.class);
	}
	
	private long getUserProfileElementTypeId(UserProfileElement profileElement) {
		try {
			return this.jdbcTemplate.queryForObject("select id from element_types where element_type = ?", new Object[]{profileElement.getTypeString()}, Long.class);
		} catch (EmptyResultDataAccessException e) {
			addProfileElementTypeToDatabase(profileElement.getTypeString());
			return this.jdbcTemplate.queryForObject("select id from element_types where element_type = ?", new Object[]{profileElement.getTypeString()}, Long.class);
		}	
	}

	

	private long getUserProfileElementNameId(UserProfileElement profileElement) {
		try {
			return this.jdbcTemplate.queryForObject("select id from user_profile_element_names where element_name = ?", new Object[]{profileElement.getNameString()}, Long.class);
		} catch (EmptyResultDataAccessException e) {
			addProfileElementNameToDatabase(profileElement.getNameString());
			return this.jdbcTemplate.queryForObject("select id from user_profile_element_names where element_name = ?", new Object[]{profileElement.getNameString()}, Long.class);
		}		
	}	

	private Long getAuthorityId(AuthorityName authority) {
		try {
			return this.jdbcTemplate.queryForObject("select id from authorities where name = ?", new Object[]{authority.name()}, Long.class);
		} catch (EmptyResultDataAccessException e) {
			addAuthorityToDatabase(authority);
			return this.jdbcTemplate.queryForObject("select id from authorities where name = ?", new Object[]{authority.name()}, Long.class);
		}		
	}	

	private List<Authority> getUserAuthorities(long userId) {
		return this.jdbcTemplate.query("select auth.id, auth.name from authorities as auth JOIN user_authorities as ua on ua.user_id=? and ua.authority_id=auth.id ", new Object[] {userId}, this::mapAuthorities);
	}

	private List<UserProfileElement> findPrivateUserProfileElements(long userId) {
		return this.jdbcTemplate.query("select element_name_id, element_type_id, element_value, is_public from user_profile_elements where user_id = ?", new Object[] {userId}, this::mapUserProfileElements);
	}
	
	private ElementType findElementType(int elementTypeId) {
		String name = this.jdbcTemplate.queryForObject("select element_type from element_types where id = ?", new Object[] {elementTypeId}, String.class);
		return ElementType.valueOf(name);
	}

	private UserProfileElementName findUserProfileElementName(int elementNameId) {
		String name = this.jdbcTemplate.queryForObject("select element_name from user_profile_element_names where id = ?", new Object[] {elementNameId}, String.class);
		return UserProfileElementName.valueOf(name);
	}	

	private User mapUser(ResultSet rs, int rowNum) throws SQLException {
		List<Authority> userAuthorites = getUserAuthorities(rs.getLong("id"));
		return new User(rs.getLong("id"), rs.getString("username"), rs.getString("email"), rs.getBoolean("enabled"), userAuthorites, rs.getString("password"));
	}

	private UserProfileElement mapUserProfileElements(ResultSet rs, int rowNum) throws SQLException {
		UserProfileElementName elementName = findUserProfileElementName(rs.getInt("element_name_id"));
		ElementType elementType = findElementType(rs.getInt("element_type_id"));
		String elementValue = rs.getString("element_value");
		boolean isPublic = rs.getBoolean("is_public");
		
		return new UserProfileElement(elementName, elementType, elementValue, isPublic);
	}
	
	private ResponseElement mapFriendsIdsToResponseElements (ResultSet rs, int rowNum) throws SQLException {		
		return new ResponseElement(ElementType.USER_ID, rs.getInt("friendId"));
	}

	private Authority mapAuthorities(ResultSet rs, int rowNum) throws SQLException {		
		return new Authority(rs.getLong("id"), AuthorityName.valueOf(rs.getString("name")));
	}	

}

package emesch.edu.repositories;

import java.util.List;

import org.springframework.jdbc.support.KeyHolder;

import emesch.edu.model.course.Chapter;
import emesch.edu.model.course.Lesson;

public interface LessonRepository {
	
	long createNewLessonAndReturnLessonId(String title, String description);
	KeyHolder createNewChapter(String chapterTitle);
	void addChapterToCourse(KeyHolder newCourseKeyHolder, KeyHolder newChapterKeyHolder);
	void addLessonToChapter(long chapterId, long lessonId);
	List<Chapter> findAllChaptersOfCourse(long courseId);
	Chapter findFirstChapterOfCourse(long courseId);
	List<Lesson> findAllChapterLessons(long chapterId);
	Lesson findLessonById(long lessonId);	

}

package emesch.edu.repositories;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import emesch.edu.model.webinar.Webinar;
import emesch.edu.utils.TableNames;

@Repository
public class JdbcWebinarRepository implements WebinarRepository {	

	private JdbcTemplate jdbcTemplate;
	
	private final String SQL_FIND_WEBINAR_BY_ID = "select id, webinar_creator_id, title, description, start_time, end_time from "+TableNames.WEBINARIUM+" where id = ?";
	
	private final String SQL_FIND_WEBINARS_ONLINE = "select * from "+TableNames.WEBINARIUM+" where start_time is not null and end_time is null limit ?";
	
	private final String SQL_FIND_WEBINARS_OFFLINE = "select * from "+TableNames.WEBINARIUM+" where start_time is not null and end_time is not null limit ?";
	
	private final String SQL_INSERT_NEW_WEBINAR = "insert into "+TableNames.WEBINARIUM+"(webinar_creator_id, title, description, start_time) values(?, ?, ?, ?)";
	
	@Autowired	
	public JdbcWebinarRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public Webinar findWebinarById(Long webinarId) {
		return this.jdbcTemplate.queryForObject(SQL_FIND_WEBINAR_BY_ID, new Object[] {webinarId}, this::mapWebinar);
	}
	
	@Override
	public List<Webinar> findWebinarsOnline(int popularQueryLimit) {
		this.jdbcTemplate.query(SQL_FIND_WEBINARS_ONLINE, new Object[] {popularQueryLimit}, this::mapWebinar);
		return null;
	}

	@Override
	public Webinar startWebinarAndReturnUpdated(Webinar webinar) {
		// TODO Auto-generated method stub
		return null;
	}		

	@Override
	public List<Webinar> getScheduledWebinarsToStart() {
		return Collections.emptyList();
	}
	
	@Override
	public List<Webinar> getWebinarsHistory(int popularQueryLimit) {
		return this.jdbcTemplate.query(SQL_FIND_WEBINARS_OFFLINE, new Object[] {popularQueryLimit}, this::mapWebinar);
	}

	@Override
	public Webinar createAndReturnNewWebinar(Long webinarCreatorId, String title, String description, boolean autostart) {
		KeyHolder newWebinarKeyHolder = new GeneratedKeyHolder();
		this.jdbcTemplate.update((connection) -> {
			Timestamp startTime = autostart == true ? new Timestamp(new Date().getTime()) : null;
			PreparedStatement ps = connection.prepareStatement(SQL_INSERT_NEW_WEBINAR);
			ps.setLong(1, webinarCreatorId);
			ps.setString(2, title);
			ps.setString(3, description);			
			ps.setTimestamp(4, startTime);
			return ps;
		}, newWebinarKeyHolder);
		return findWebinarById(newWebinarKeyHolder.getKey().longValue());		
	}
	
	private Webinar mapWebinar(ResultSet rs, int rowNum) throws SQLException {
		return new Webinar(rs.getLong("id"), rs.getLong("webinar_creator_id"), rs.getString("title"), rs.getString("description"), rs.getTimestamp("start_time"), rs.getTimestamp("end_time"));
	}		
		
}

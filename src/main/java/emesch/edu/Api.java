package emesch.edu;

public class Api {

	public static final String API = "/api";
	public static final String PUBLIC_USERS = API+"/users";	 
	public static final String PUBLIC_SUFFIX_REGISTER_USER = "/register";
	public static final String PUBLIC_REGISTER_USER = PUBLIC_USERS+PUBLIC_SUFFIX_REGISTER_USER;
	
	public static final String PUBLIC_USER_PROFILE = "/user";
	public static final String PUBLIC_SUFFIX_GET_FRIENDS = "/friends";
	public static final String PUBLIC_GET_FRIENDS = PUBLIC_USER_PROFILE+"/{id}"+PUBLIC_SUFFIX_GET_FRIENDS;
	
	public static final String GET_ALL_COURSES = "/api/courses";
	public static final String GET_MY_COURSES = "/api/courses/my";
	public static final String GET_POPULAR_COURSES = "/api/courses/popular";
	public static final String CREATE_NEW_COURSE = GET_ALL_COURSES+"/new";
	
	public static final String CREATE_NEW_COURSE_CHAPTER = GET_ALL_COURSES+"/{courseId}"+"/chapters"+"/new";
	
	public static final String CREATE_NEW_COURSE_LESSON = "/api/lessons/new";
	
	public static final String GET_WEBINARS_ONLINE = "/api/webinar/online";
	public static final String GET_WEBINARS_OFFLINE = "/api/webinar/offline";
	public static final String CREATE_NEW_WEBINAR = "/api/webinar/new";
	public static final String START_EXISTING_WEBINAR = "/api/webinar/{webinarId}/start";
	public static final String JOIN_WEBINAR = "/api/webinar/{webinarId}";
	
	public static final String PRIVATE = "/prv";	
	public static final String PRIVATE_GET_CURRENT_USER = PRIVATE+"/current";	
	
	public static final String ADMIN = "/adm";
	public static final String ADMIN_GET_REGISTERED_USERS = ADMIN+"/users/registered";
		
	
	public static String getPublicUserProfile(long otherUserId) {
		return PUBLIC_USER_PROFILE+"/"+otherUserId;
	}
	
	public static String getFriendsOfUser(long otherUserId) {
		return PUBLIC_USER_PROFILE+"/"+otherUserId+PUBLIC_SUFFIX_GET_FRIENDS;
	}

	public static String registerUser() {
		return PUBLIC_USERS+PUBLIC_SUFFIX_REGISTER_USER;
	}
	
	/*
	 * PRIVATE API
	 */
	
	public static String getCurrentUser() {
		return PRIVATE_GET_CURRENT_USER;
	}
	
	public static String getPrivateContent(long currentUserId) {
		return PRIVATE+"/"+currentUserId;
	}
	
	/*
	 * ADMIN API
	 */

	public static String adminGetRegisteredUsers() {
		return ADMIN_GET_REGISTERED_USERS;		
	}
	
	
}

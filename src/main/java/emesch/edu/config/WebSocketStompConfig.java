package emesch.edu.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

//@Configuration
//@EnableWebSocketMessageBroker
public class WebSocketStompConfig extends AbstractWebSocketMessageBrokerConfigurer {
	
	public static final String ENDPOINT_TEST = "/testweb";

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint(ENDPOINT_TEST).withSockJS();		
	}
	
	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {		
		registry.setApplicationDestinationPrefixes("/app");
		//registry.enableSimpleBroker("/queue", "/topic");
		registry.enableStompBrokerRelay("/topic", "/queue");		
	}

}

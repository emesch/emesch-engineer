package emesch.edu.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import emesch.edu.Api;
import emesch.edu.security.BadRequestFilter;
import emesch.edu.security.JwtAuthenticationEntryPoint;
import emesch.edu.security.JwtAuthenticationTokenFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired	
	private JwtAuthenticationEntryPoint unauthorizedHandler;

	@Autowired
	private UserDetailsService userDetailsService;

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public JwtAuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
		return new JwtAuthenticationTokenFilter();
	}

	@Bean
	public BadRequestFilter badRequestFilterBean() throws Exception {
		return new BadRequestFilter();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder authManagerBuilder) throws Exception {
		authManagerBuilder
		.userDetailsService(this.userDetailsService)
		.passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {

		httpSecurity
		// we don't need CSRF protection because our token is invulnerable
		.csrf().disable()
		.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
		.authorizeRequests()
		.antMatchers(
				HttpMethod.GET,
				"/",
				"/*.html",
				"/**/*.html",
				"/**/*.css",
				"/**/*.js",
				"/**/*.json",
				"/**/img/**",
				"/h2-console/",
				"/h2-console/*",
				Api.API+"/*",
				Api.PUBLIC_USER_PROFILE+"/*",
				Api.GET_ALL_COURSES,
				Api.GET_ALL_COURSES+"/*",
				Api.GET_WEBINARS_ONLINE,
				Api.GET_WEBINARS_OFFLINE
				).permitAll()
		.antMatchers(
				HttpMethod.POST,
				Api.PUBLIC_REGISTER_USER				
				).permitAll()
		.antMatchers("/auth/**").permitAll()
		.anyRequest().authenticated();

		httpSecurity.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);
		httpSecurity.addFilterBefore(badRequestFilterBean(), JwtAuthenticationTokenFilter.class);

		// disable page caching
		httpSecurity.headers().cacheControl();		

	}

}

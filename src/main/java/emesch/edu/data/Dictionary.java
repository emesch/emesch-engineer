package emesch.edu.data;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * 
 * Need to be implemented in future, or not. 
 *
 */

@Deprecated
public abstract class Dictionary {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	protected Dictionary() {
		if(isExistingTable()){
			initializeDictionaryInDatabase(getQueryForDictionaryUpdate(), getDictionaryValues());	
		}			
	}

	private boolean isExistingTable() {
		return false;
	}

	protected abstract List<Dictionary> getDictionaryValues();
	protected abstract String getQueryForDictionaryUpdate();

	private void initializeDictionaryInDatabase(String sql, List<Dictionary> batchArgs) {
		//this.jdbcTemplate.update(sql, args)
	}


}



package emesch.edu.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import emesch.edu.data.error.ResponseError;

public class ResponseData implements Serializable {
	
	public static final String PARAM_VIEW = "view";
	public static final String PARAM_ELEMENTS = "elements";
	public static final String PARAM_ERRORS = "errors";
	
	public static final String ELEMENT_USER_INFO = "userInfo";
	
	
	private ResponseView view;
	private Map<String, ResponseElement> elements;
	private List<ResponseError> errors;
	
	public ResponseData() {
		this.view = null;
		this.elements = new HashMap<>();
		this.errors = new ArrayList<>();
	}
	
	public ResponseView getView() {
		return view;
	}

	public void setView(ResponseView view) {
		this.view = view;
	}	

	public Map<String, ResponseElement> getElements() {
		return elements;
	}

	public void setElements(Map<String, ResponseElement> elements) {
		this.elements = elements;
	}

	public List<ResponseError> getErrors() {
		return errors;
	}

	public void setErrors(List<ResponseError> errors) {
		this.errors = errors;
	}

	public void addElement(String responseElementName, ResponseElement responseElement) {
		this.elements.put(responseElementName, responseElement);		
	}
	
	public void addAll(Map<String, ResponseElement> responseElements) {
		this.elements.putAll(responseElements);
	}

}

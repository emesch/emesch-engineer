package emesch.edu.data.error;

import java.io.Serializable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ResponseError implements Serializable {
	
	public static final String PARAM_NAME = "error";
	public static final String PARAM_MSG = "Message";
	
	private ResponseErrorEnum error;
	private String message;	

	public ResponseError(ResponseErrorEnum responseEnum, String message) {
		super();
		this.error = responseEnum;
		this.message = message;
	}

	public static String getJsonError(ResponseErrorEnum responseEnum, String message) {		
		try {
			return new ObjectMapper().writeValueAsString(new ResponseError(responseEnum, message));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "";
		}
	}	

	public ResponseErrorEnum getError() {
		return error;
	}

	public String getMessage() {
		return message;
	}	

}

package emesch.edu.data;

public class ResponseDataElementKeys {

	public static final String VALIDATION_ERROR_LIST = "validationErrorList";
	
	public static final String USER_FRIENDS_COUNT = "friendsCount";
	public static final String USER_FRIENDS_ID_LIST = "friendsIds";
	public static final String REGISTERED_USER_ID = "registeredUserId";
	public static final String INSTRUCTOR_LIST = "instructors";
	
	public static final String NEW_COURSE_URL = "newCourseUrl";
	public static final String COURSE_OBJECT = "courseObject";
	public static final String COURSES_LIST = "courses";
	public static final String COURSE_LESSONS_LIST = "lessons";
	
	public static final String NEW_LESSON_URL = "newLessonUrl";
	public static final String LESSON_OBJECT = "lessonObject";

	public static final String WEBINARS_ONLINE = "webinarsOnline";
	public static final String WEBINARS_OFFLINE = "webinarsOffline";
	public static final String WEBINAR_OBJECT = "webinarObject";
				

}

package emesch.edu.data;

public enum ElementType {
	
	TEXT, EMAIL,
	
	NUMBER_LONG,
	
	URL, REDIRECT_URL, API_URL,	
	
	USER_ID,
	
	COURSE_OBJECT,
	
	LESSON_OBJECT,
	
	WEBINAR_OBJECT,
	
	IMG_AVATAR, 
	
	LIST_IDS, LIST_OBJECT	

}

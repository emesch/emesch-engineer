package emesch.edu.data;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ResponseElement implements Serializable {
	
	private static final long serialVersionUID = 5437470180724704411L;
	
	private ElementType type;
	private Object value;	
	
	public ResponseElement(ElementType type, Object value) {
		this.type = type;
		this.value = value;
	}

	public ElementType getType() {
		return type;
	}

	public void setType(ElementType type) {
		this.type = type;
	}
	
	@JsonIgnore
	public String getStringTypeValue() {
		return type.toString();
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}	

}

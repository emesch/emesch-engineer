package emesch.edu.model.webinar;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import emesch.edu.model.users.User;

public class Webinar {
	
	private long id;
	private long webinarCreatorId; 
	private String title;
	private String description;
	private Timestamp startTime;
	private Timestamp endTime;
	private Map<Long, User> usersJoined; 
	
	public Webinar(long id, long webinarCreatorId, String title, String description, Timestamp startTime, Timestamp endTime) {		
		this.id = id;
		this.webinarCreatorId = webinarCreatorId;
		this.title = title;
		this.description = description;
		this.startTime = startTime;
		this.endTime = endTime;
		this.usersJoined = new HashMap<>();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getWebinarCreatorId() {
		return webinarCreatorId;
	}

	public void setWebinarCreatorId(long webinarCreatorId) {
		this.webinarCreatorId = webinarCreatorId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getStartTime() {
		return startTime;
	}

	public void setStartTime(Timestamp startTime) {
		this.startTime = startTime;
	}

	public Timestamp getEndTime() {
		return endTime;
	}

	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}	
	

	public Map<Long, User> getUsersJoined() {
		return usersJoined;
	}

	public void joinUser(User user) {
		usersJoined.put(user.getId(), user);		
	}		
		
	
}

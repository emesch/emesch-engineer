package emesch.edu.model.users;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class User {

	private Long id;    
	private String username;
	private String email;    
	private boolean enabled;    
	private List<Authority> authorities;
	private String password;

	public User(Long id, String username, String email, boolean enabled, List<Authority> authorities, String password) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.enabled = enabled;
		this.authorities = authorities;
		this.password = password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}  

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean getEnabled() {
		return enabled;
	}	

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<Authority> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<Authority> authorities) {
		this.authorities = authorities;
	}
	
	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}	

}
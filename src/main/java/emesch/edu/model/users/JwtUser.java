package emesch.edu.model.users;

import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JwtUser implements UserDetails{

	private final Long id;
	private final String email;
	private final String username;	 
	private final Collection<? extends GrantedAuthority> authorities;	 
	private final boolean enabled;
	private final String password;

	public JwtUser(Long id, String email, String username, Collection<? extends GrantedAuthority> authorities, boolean enabled, String password){     
		this.id = id;
		this.email = email;
		this.username = username;		        
		
		this.authorities = authorities;
		this.enabled = enabled;
		
		this.password = password;
	}



	@JsonIgnore 
	public Long getId() {
		return id;
	}	

	public String getEmail() {
		return email;
	}
	
	@Override
	public String getUsername() {
		return username;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@JsonIgnore
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}	

	@JsonIgnore
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}	

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}	

	@Override
	public boolean isEnabled() {
		return enabled;
	}	

	@Override
	@JsonIgnore
	public String getPassword() {
		return password;
	}

}

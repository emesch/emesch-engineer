package emesch.edu.model.users;

public enum AuthorityName {
	ROLE_USER, ROLE_ADMIN
}

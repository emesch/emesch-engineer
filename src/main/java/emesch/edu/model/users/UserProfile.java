package emesch.edu.model.users;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;

import emesch.edu.data.ResponseElement;

/**
 * Each UserProfile can be composed individually by adding chosen UserProfileElements.
 * Sometimes we need to show all possible UserProfileElements, but sometimes it can be limited.  
 */
public class UserProfile {
	
	private long userId;
	private List<UserProfileElement> elements;

	public UserProfile(long userId, List<UserProfileElement> elements) {
		this.userId = userId;
		this.elements = elements;
	}	

	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public List<UserProfileElement> getElements() {
		return elements;
	}
	
	@JsonIgnore
	public Map<String, ResponseElement> getResponseElements() {
		Map<String, ResponseElement> responseElements = new HashMap<>();
		for (UserProfileElement profileElement: getElements()) {
			ResponseElement responseElement = new ResponseElement(profileElement.getType(), profileElement.getValue());
			responseElements.put(responseElement.getStringTypeValue(), responseElement);
		}
		return responseElements;
	}	

}

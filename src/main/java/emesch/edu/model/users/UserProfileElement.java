package emesch.edu.model.users;

import com.fasterxml.jackson.annotation.JsonIgnore;

import emesch.edu.data.ElementType;

public class UserProfileElement {
	
	private UserProfileElementName name;
	private ElementType type;
	private String value;
	private boolean publicValue;
	
	public UserProfileElement(UserProfileElementName name, ElementType type, String value, boolean isPublic) {
		this.name = name;
		this.type = type;
		this.value = value;
		this.publicValue = isPublic;
	}

	public UserProfileElementName getName() {
		return name;
	}

	public void setName(UserProfileElementName name) {
		this.name = name;
	}

	public ElementType getType() {
		return type;
	}

	public void setType(ElementType type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isPublicValue() {
		return publicValue;
	}

	public void setPublicValue(boolean publicValue) {
		this.publicValue = publicValue;
	}

	@JsonIgnore
	public String getNameString() {
		return getName().name();
	}
	
	@JsonIgnore
	public String getTypeString() {
		return getType().name();
	}	

}

package emesch.edu.model.course;

import java.time.Duration;

public class Lesson {
	
	private long id;
	private String title;
	private Duration duration;
	
	
	public Lesson(long id, String title, Duration duration) {		
		this.id = id;
		this.title = title;
		this.duration = duration;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Duration getDuration() {
		return duration;
	}
	public void setDuration(Duration duration) {
		this.duration = duration;
	}	

}

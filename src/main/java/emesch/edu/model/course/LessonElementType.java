package emesch.edu.model.course;

public enum LessonElementType {
	
	NOTE, AUDIO, VIDEO

}

package emesch.edu.model.course;

public class NoteLessonElement implements LessonElement {	

	@Override
	public LessonElementType getType() {
		return LessonElementType.NOTE;
	}

}

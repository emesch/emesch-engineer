package emesch.edu.model.course;

public class VideoLessonElement implements LessonElement {

	@Override
	public LessonElementType getType() {
		return LessonElementType.VIDEO;		
	}

}

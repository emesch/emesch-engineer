package emesch.edu.model.course;

import java.util.ArrayList;
import java.util.List;

import emesch.edu.model.users.UserProfile;

public class Course {
	
	private long id;
	private String title;
	private long creatorId;
	private long rating;
	private long votes;
	private CourseAccess access;
	List<UserProfile> instructors;
	List<Chapter> chapters;
	
	public Course(long id, String title, long creatorId, CourseAccess access, List<UserProfile> instructors) {
		this(id, title, creatorId, access, 0, 0);
	}

	public Course(long id, String title, long creatorId, CourseAccess access, long rating, long votes) {
		this.id = id;
		this.title = title;
		this.creatorId = creatorId;
		this.access = access;
		this.rating = rating;
		this.votes = votes;
		this.instructors = new ArrayList<UserProfile>();
		this.chapters = new ArrayList<Chapter>();
	}
	
	public void addInstructors(List<UserProfile> instructors) {
		getInstructors().addAll(instructors);
	}
	
	public void addChapters(List<Chapter> chapters) {
		getChapters().addAll(chapters);
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(long creatorId) {
		this.creatorId = creatorId;
	}

	public CourseAccess getAccess() {
		return access;
	}
	public void setAccess(CourseAccess access) {
		this.access = access;
	}
	public long getRating() {
		return rating;
	}
	public void setRating(long rating) {
		this.rating = rating;
	}
	public long getVotes() {
		return votes;
	}
	public void setVotes(long votes) {
		this.votes = votes;
	}
	public List<UserProfile> getInstructors() {
		return instructors;
	}
	public void setInstructors(List<UserProfile> instructors) {
		this.instructors = instructors;
	}

	public List<Chapter> getChapters() {
		return chapters;
	}

	public void setChapters(List<Chapter> chapters) {
		this.chapters = chapters;
	}	
	

}

package emesch.edu.model.course;

import java.util.List;

public class Chapter {
	
	private Long id;
	private String title;
	private List<Lesson> lessons;
	
	public Chapter(Long id, String title, List<Lesson> lessons) {
		super();
		this.id = id;
		this.title = title;
		this.lessons = lessons;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public List<Lesson> getLessons() {
		return lessons;
	}
	public void setLessons(List<Lesson> lessons) {
		this.lessons = lessons;
	}

}

package emesch.edu.model.course;

public enum CourseAccess {
	
	PUBLIC, REGISTERED_USERS, INVITED_USERS

}

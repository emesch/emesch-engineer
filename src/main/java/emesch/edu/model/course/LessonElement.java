package emesch.edu.model.course;

public interface LessonElement {
	
	LessonElementType getType();

}

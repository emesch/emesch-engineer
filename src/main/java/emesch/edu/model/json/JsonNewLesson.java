package emesch.edu.model.json;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("serial")
public class JsonNewLesson implements Serializable {
	
	@JsonProperty
	@NotNull
	private Long courseId;
	
	@JsonProperty
	@NotNull
	private Long chapterId;
	
	@JsonProperty
	@NotNull
	private String title;
	
	@JsonProperty
	@NotNull
	private String description;	
	
	public JsonNewLesson() {
		
	}	
	
	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Long getChapterId() {
		return chapterId;
	}
	
	public void setChapterId(Long chapterId) {
		this.chapterId = chapterId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	

}

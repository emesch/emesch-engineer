package emesch.edu.schedulers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import emesch.edu.model.webinar.Webinar;
import emesch.edu.services.WebinarService;

/**
 * This scheduler should run every minute to check in database which webinars should start in this minute 
 */
@Component
public class WebinarScheduler {

	private WebinarService webinarService;
	
	@Autowired	
	public WebinarScheduler(WebinarService webinarService) {
		this.webinarService = webinarService;
	}
	
	@Scheduled(fixedRate=60000)
	public void startScheduledWebinars() {
		this.webinarService.startScheduledWebinars();
	}

}

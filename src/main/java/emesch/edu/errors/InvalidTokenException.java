package emesch.edu.errors;

import org.springframework.security.core.AuthenticationException;

public class InvalidTokenException extends AuthenticationException {
	
	public static final String BEARER_ABSENCE = "Invalid token header - Bearer expected.";

	public InvalidTokenException(String msg) {
		super(msg);
	}

}

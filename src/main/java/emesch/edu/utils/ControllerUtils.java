package emesch.edu.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;

import emesch.edu.data.ElementType;
import emesch.edu.data.ResponseData;
import emesch.edu.data.ResponseDataElementKeys;
import emesch.edu.data.ResponseElement;
import emesch.edu.data.error.ResponseError;
import emesch.edu.data.error.ResponseErrorEnum;

public class ControllerUtils {

	public static ResponseEntity<ResponseData> validationError(String errorMsg, BindingResult bindingResult) {		
		HttpStatus status = HttpStatus.BAD_REQUEST;
		ResponseData responseData = new ResponseData();
		List<ResponseError> responseErrors = new ArrayList<>();
		responseErrors.add(new ResponseError(ResponseErrorEnum.VALIDATION_FAILED, errorMsg));
		responseData.addElement(ResponseDataElementKeys.VALIDATION_ERROR_LIST, new ResponseElement(ElementType.LIST_OBJECT, bindingResult.getAllErrors()));
		responseData.setErrors(responseErrors);
		return new ResponseEntity<>(responseData, status);
	}

	public static ResponseEntity<ResponseData> creationSuccess(String responseDataElementKey, String newObjectUrl) {
		HttpStatus status = HttpStatus.CREATED;
		ResponseData responseData = new ResponseData();
		List<ResponseError> responseErrors = new ArrayList<>();
		responseData.addElement(responseDataElementKey, new ResponseElement(ElementType.API_URL, newObjectUrl));
		return new ResponseEntity<>(responseData, status);
	}

	public static ResponseEntity<ResponseData> creationFailed(String errorMsg, Logger logger) {	
		logger.error(errorMsg);
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;		
		ResponseData responseData = new ResponseData();
		List<ResponseError> responseErrors = new ArrayList<>();				
		responseErrors.add(new ResponseError(ResponseErrorEnum.OBJECT_NOT_CREATED, errorMsg));
		responseData.setErrors(responseErrors);		
		return new ResponseEntity<>(responseData, status);
	}

}

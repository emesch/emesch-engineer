package emesch.edu.utils;

public class TableNames {
	
	/** Contains course id, title, and creator id. */
	public static final String COURSES = "courses";
	
	/** Contains chapter id, title */
	public static final String CHAPTERS = "chapters"; 
	
	/** Contains id and title of independent lessons. */	
	public static final String LESSONS = "lessons";
	
	/** Contains relations between courses and instructors. */
	public static final String COURSES_INSTRUCTORS = "courses_instructors";	
	
	/** Contains relations between courses and chapters. */
	public static final String COURSES_CHAPTERS = "courses_chapters";
	
	/** Contains relations between chapters and lessons. */
	public static final String CHAPTERS_LESSONS = "chapters_lessons";

	/** Contains id, webinar_creator_id, title, description, start_time, end_time */
	public static final String WEBINARIUM = "webinarium";

}

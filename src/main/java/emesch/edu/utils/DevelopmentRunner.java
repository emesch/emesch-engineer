package emesch.edu.utils;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import emesch.edu.model.json.JsonNewWebinar;
import emesch.edu.model.users.User;
import emesch.edu.services.CourseService;
import emesch.edu.services.UserService;
import emesch.edu.services.WebinarService;

@Component
@Profile("development")
public class DevelopmentRunner {
	
	private static final String DEV_USER_NAME =  "devRunner";
	
	private User devUser;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CourseService courseService;
	
	@Autowired
	private WebinarService webinarService;
	
	private static Logger logger = Logger.getLogger(DevelopmentRunner.class.getName());
	
	@PostConstruct
	public void init() {
		logger.info("DevelopmentRunner initialization...");
		createTestUsers();
		createTestCourses();
		addTestLessons();
		createTestWebinars();
	}
	
	private void createTestUsers() {
		logger.info("Creating test users.");
		this.userService.registerUser(DEV_USER_NAME, "dev@emesch.pl", "test123");
		this.devUser = this.userService.findUserByUsername(DEV_USER_NAME);
	}
	
	private void createTestCourses() {
		logger.info("Creating test courses.");		
		long testCourse1Id = this.courseService.createNewCourseAndReturnCourseId(devUser.getId(), "Dev Course 1", "Test description", courseService.getDefaultScope());
		long testCourse2Id = this.courseService.createNewCourseAndReturnCourseId(devUser.getId(), "Dev Course 2", "Test description", courseService.getDefaultScope());
		long testCourse3Id = this.courseService.createNewCourseAndReturnCourseId(devUser.getId(), "Dev Course 3", "Test description", courseService.getDefaultScope());
		long testCourse4Id = this.courseService.createNewCourseAndReturnCourseId(devUser.getId(), "Dev Course 4", "Test description", courseService.getDefaultScope());
		long testCourse5Id = this.courseService.createNewCourseAndReturnCourseId(devUser.getId(), "Dev Course 5", "Test description", courseService.getDefaultScope());	
	
		logger.info("Adding test lessons.");		
		Long testLesson1Id = this.courseService.createNewLessonAndReturnLessonId("Lekcja testowa1", "Opis testowy1");
		Long testLesson2Id = this.courseService.createNewLessonAndReturnLessonId("Lekcja testowa2", "Opis testowy2");
		Long testLesson3Id = this.courseService.createNewLessonAndReturnLessonId("Lekcja testowa3", "Opis testowy3");
		Long testLesson4Id = this.courseService.createNewLessonAndReturnLessonId("Lekcja testowa4", "Opis testowy4");
		
		this.courseService.addLessonToDefaultChapter(testCourse1Id, testLesson1Id);	
		this.courseService.addLessonToDefaultChapter(testCourse1Id, testLesson2Id);	
		this.courseService.addLessonToDefaultChapter(testCourse2Id, testLesson3Id);	
		this.courseService.addLessonToDefaultChapter(testCourse2Id, testLesson4Id);	
	}
	
	private void addTestLessons() {
		
	}

	private void createTestWebinars() {
		this.webinarService.createAndReturnNewWebinar(devUser.getId(), "Test started webinar", "Webinar should be online.", true);
		this.webinarService.createAndReturnNewWebinar(devUser.getId(), "Test not started webinar", "Webinar should be offline", false);
	}
	
	

}

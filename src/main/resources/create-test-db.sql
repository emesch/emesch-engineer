CREATE TABLE users (
  id bigint auto_increment NOT NULL PRIMARY KEY,
  username VARCHAR(50) NOT NULL unique,
  email VARCHAR(50) NOT NULL unique,
  enabled boolean not null default false,
  password VARCHAR(250)  
);

CREATE TABLE authorities (
  id bigint auto_increment not null PRIMARY KEY,
  name VARCHAR(20) not null unique
);

CREATE TABLE user_authorities (  
  user_id bigint NOT NULL,
  authority_id bigint NOT NULL,
  FOREIGN KEY(user_id) REFERENCES users(id),
  FOREIGN KEY(authority_id) REFERENCES authorities(id),
  PRIMARY KEY(user_id, authority_id)
);

CREATE TABLE user_profile_element_names (
  id bigint auto_increment NOT NULL PRIMARY KEY,
  element_name VARCHAR(100) NOT NULL unique
);

CREATE TABLE element_types (
  id bigint auto_increment NOT NULL PRIMARY KEY,
  element_type VARCHAR(70) NOT NULL unique
);

CREATE TABLE user_profile_elements (
  user_id bigint NOT NULL,
  element_name_id bigint NOT NULL,
  element_type_id bigint NOT NULL,
  element_value VARCHAR(250),
  is_public boolean NOT NULL default false,
  FOREIGN KEY(element_name_id) REFERENCES user_profile_element_names(id),
  FOREIGN KEY(element_type_id) REFERENCES element_types(id)
);

CREATE TABLE user_invitations (
  inviting_user_id bigint NOT NULL,
  invited_user_id bigint NOT NULL,  
  invitation_sended TIMESTAMP,
  invitation_accepted TIMESTAMP,
  FOREIGN KEY(inviting_user_id) REFERENCES users(id),
  FOREIGN KEY(invited_user_id) REFERENCES users(id),
  PRIMARY KEY(inviting_user_id, invited_user_id)
);

CREATE TABLE courses (
  id bigint auto_increment NOT NULL PRIMARY KEY,
  title VARCHAR(100) NOT NULL,
  course_creator_id bigint NOT NULL, 
  rating bigint NOT NULL default 0,  
  votes bigint NOT NULL default 0,
  FOREIGN KEY(course_creator_id) REFERENCES users(id),
);

CREATE TABLE lessons (
  id bigint auto_increment NOT NULL PRIMARY KEY,
  title VARCHAR(100) NOT NULL,
  description VARCHAR(255)  		
);

CREATE TABLE chapters (
	id bigint auto_increment NOT NULL PRIMARY KEY,
	title VARCHAR(100) NOT NULL	
);

CREATE TABLE courses_instructors (
  course_id bigint NOT NULL,
  instructor_id bigint NOT NULL,
  FOREIGN KEY(course_id) REFERENCES courses(id),
  FOREIGN KEY(instructor_id) REFERENCES users(id)
);

CREATE TABLE courses_chapters (
	course_id bigint NOT NULL,
	chapter_id bigint NOT NULL,
	FOREIGN KEY(course_id) REFERENCES courses(id),
  	FOREIGN KEY(chapter_id) REFERENCES chapters(id)		
);

CREATE TABLE chapters_lessons (
  chapter_id bigint NOT NULL,
  lesson_id bigint NOT NULL,  
  FOREIGN KEY(chapter_id) REFERENCES chapters(id),
  FOREIGN KEY(lesson_id) REFERENCES lessons(id)
);

CREATE TABLE course_details (
	course_id bigint NOT NULL,
	description VARCHAR(255), 
	FOREIGN KEY(course_id) REFERENCES courses(id)  	
);

CREATE TABLE webinarium (
    id bigint auto_increment NOT NULL PRIMARY KEY,
    webinar_creator_id bigint NOT NULL,
    title VARCHAR(100) NOT NULL,
    description VARCHAR(255), 
    start_time TIMESTAMP,
    end_time TIMESTAMP,
    FOREIGN KEY(webinar_creator_id) REFERENCES users(id)
);
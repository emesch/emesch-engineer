package emesch.edu.security;

import java.io.Serializable;

public class TokenObject implements Serializable{	
	
	private static final long serialVersionUID = 2649302071480587904L;
	
	private String token;
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}	

}

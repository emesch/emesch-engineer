package emesch.edu.security;

import javax.servlet.Filter;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import emesch.edu.Api;
import emesch.edu.data.error.ResponseError;
import emesch.edu.data.error.ResponseErrorEnum;
import emesch.edu.repositories.UserRepository;
import emesch.edu.utils.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class AuthenticationTest {

	public static final String AUTHORIZATION_HEADER = "Authorization";
		
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;
	@Autowired
	private Filter springSecurityFilterChain;	

	public static final String AUTHENTICATION_PATH = "/auth";

	@Before
	public void setup() {		
		this.mockMvc = MockMvcBuilders
				.webAppContextSetup(this.wac)
				.addFilters(this.springSecurityFilterChain)				
				.build();	
	}
	
	/**
	 * Should get jsonToken for testUser - token: xxxx Return authorization header with Bearer. 	 
	 */
	@Test
	public void shouldGetAuthenticationToken() throws Exception {		
		TestUtils.authenticateTestUserAndReturnAuthorizationHeader(this.mockMvc, true);	
	}	

	/**
	 * Request public API without authorization header. 
	 * Expecting status OK. 	 
	 */
	@Test	
	public void shouldPassGuestToPublicContent() throws Exception {		
		this.mockMvc.perform(MockMvcRequestBuilders.get("/"))			
		.andExpect(MockMvcResultMatchers.status().isOk());
	}

	/**
	 * Tries to request private api without authorization header. 
	 * Expecting status UNAUTHORIZED. 
	 */
	@Test
	public void shouldNotPassGuestToClosedContent() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders.get(Api.adminGetRegisteredUsers()))			
		.andExpect(MockMvcResultMatchers.status().isUnauthorized());
		
		this.mockMvc.perform(MockMvcRequestBuilders
				.get(Api.getCurrentUser()))		
		.andExpect(MockMvcResultMatchers.status().isUnauthorized());
	}

	/**
	 * Gets invalid authorization header without bearer and tries to request private API.
	 * Expecting status UNAUTHORIZED. 
	 */
	@Test
	public void shouldRecognizeInvalidToken() throws Exception {
		String invalidAuthorizationHeaderWithoutBearer = TestUtils.authenticateTestUserAndReturnAuthorizationHeader(mockMvc, false);
		
		this.mockMvc.perform(MockMvcRequestBuilders
				.get(Api.getCurrentUser())
				.header(AUTHORIZATION_HEADER, invalidAuthorizationHeaderWithoutBearer))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.status().isUnauthorized())
		.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasEntry(ResponseError.PARAM_NAME, ResponseErrorEnum.INVALID_TOKEN.toString())));
	}
	
	/**
	 * Gets authorization header for test user: Bearer xxxx.
	 * Tries to request public api with authorization header. 
	 * Expecting status OK. 
	 */
	@Test
	public void shouldPassSignedUserToPublicContent() throws Exception {
		String authorizationHeader = TestUtils.authenticateTestUserAndReturnAuthorizationHeader(mockMvc, true);

		this.mockMvc.perform(MockMvcRequestBuilders
				.get("/")
				.header("Authorization", authorizationHeader))		
		.andExpect(MockMvcResultMatchers.status().isOk());
	}

	/**
	 * Gets authorization header for test user: Bearer xxxx.
	 * Tries to request private closed api with authorization header. 
	 * Expecting status OK. 
	 */
	@Test
	public void shouldPassSignedUserToClosedContent() throws Exception {
		String authorizationHeader = TestUtils.authenticateTestUserAndReturnAuthorizationHeader(this.mockMvc, true);
		
		this.mockMvc.perform(MockMvcRequestBuilders
				.get(Api.getCurrentUser())
				.header("Authorization", authorizationHeader))		
		.andExpect(MockMvcResultMatchers.status().isOk());
	}

	/**
	 * Gets authorization header for test user: Bearer xxxx.
	 * Tries to request private api with authorization header. 
	 * Expecting status OK. 
	 */
	@Test
	public void shouldPassSignedUserToHisPrivateContent() throws Exception {
		String authorizationHeader = TestUtils.authenticateTestUserAndReturnAuthorizationHeader(this.mockMvc, true);
		
		this.mockMvc.perform(MockMvcRequestBuilders
				.get(Api.getCurrentUser())
				.header("Authorization", authorizationHeader))		
		.andExpect(MockMvcResultMatchers.status().isOk());
		
		long currentUserId = this.userRepository.findUserByUsername("test").getId();
		this.mockMvc.perform(MockMvcRequestBuilders
				.get(Api.getPrivateContent(currentUserId))
				.header("Authorization", authorizationHeader))		
		.andExpect(MockMvcResultMatchers.status().isOk());
	}

	/**
	 * 
	 */
	@Test
	public void shouldNotPassSignedUserToForeignPrivateContent() throws Exception {
		long foreignUserId = this.userRepository.findUserByUsername("test2").getId();
		String jwtTestUser = TestUtils.performLoginAndGetJsonToken(this.mockMvc, TestUtils.getJsonForTestUser());
		
		this.mockMvc.perform(MockMvcRequestBuilders
				.get(Api.getPrivateContent(foreignUserId))
				.header("Authorization", jwtTestUser))		
		.andExpect(MockMvcResultMatchers.status().isUnauthorized());		
	}

	//@Test
	public void AuthoritiesTest() {
		Assert.fail("not implemented!");
	}		

}

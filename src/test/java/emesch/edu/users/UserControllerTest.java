package emesch.edu.users;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import javax.servlet.Filter;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import emesch.edu.Api;
import emesch.edu.model.users.User;
import emesch.edu.repositories.UserRepository;
import emesch.edu.utils.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class UserControllerTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;
	
	@Autowired
	private UserRepository repository;
	
	@Autowired
	private Filter springSecurityFilterChain;

	@Before
	public void setup() {		
		this.mockMvc = MockMvcBuilders
				.webAppContextSetup(this.wac)
				.addFilters(this.springSecurityFilterChain)
				.build();		
	}
	
	@Test
	@Transactional
	public void shouldGetUsersFromRepository() {
		User testUser = this.repository.findUserByUsername("test");
		assertNotNull(testUser);
		assertThat("TestUser username is test", testUser.getUsername(), CoreMatchers.is("test"));
		
		User emeschUser = this.repository.findUserByUsername("emesch");
		assertNotNull(emeschUser);
		assertThat("Emesch username is emesch", emeschUser.getUsername(), CoreMatchers.is("emesch"));
	}

	@Test
	@Transactional
	public void shouldRegisterNewUser() throws Exception {
		String username = "testUser3";
		String userEmail = "testUser3@tests.emesch.com";
		String userNonEncryptedPassword = "test123";
		
		TestUtils.performRegisterUser(this.mockMvc, username, userEmail, userNonEncryptedPassword);			
		
		User newUser = this.repository.findUserByEmail(userEmail);
		if(newUser==null){
			Assert.fail("User creation failed");
		}
			
		if(newUser.getPassword().equals(userNonEncryptedPassword)) {
			Assert.fail("User password in database is not encrypted!");
		}
	}

	@Test
	@Transactional
	public void shouldNotRegisterExistingUsername() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders
				.post(Api.registerUser())
				.param("username", "test")
				.param("email", "nottestUser@tests.emesch.com")
				.param("password", "test456"))		
		.andExpect(MockMvcResultMatchers.status().isConflict());		
	}
	
	@Test
	@Transactional
	public void shouldNotRegisterExistingEmail() throws Exception {
		this.mockMvc.perform(MockMvcRequestBuilders
				.post(Api.registerUser())
				.param("username", "siubidubi")
				.param("email", "testUser@tests.emesch.com")
				.param("password", "test456"))		
		.andExpect(MockMvcResultMatchers.status().isConflict());		
	}
	
	public void testForNotEnabledUser() {
		
	}


}

package emesch.edu.users;

import javax.servlet.Filter;

import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import emesch.edu.Api;
import emesch.edu.data.ResponseData;
import emesch.edu.data.ResponseView;
import emesch.edu.repositories.UserRepository;
import emesch.edu.utils.TestUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class UserProfileTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;
	@Autowired
	private Filter springSecurityFilterChain;

	@Autowired
	private UserRepository userRepository;

	private static final long NON_EXISTING_USER_ID = 9999;
	private long testUser2Id;
	private String newTestUsername = "user999";
	private String newTestEmail = "testUser999@emesch.com";

	@Before
	public void setup() {		
		this.mockMvc = MockMvcBuilders
				.webAppContextSetup(this.wac)
				.addFilters(this.springSecurityFilterChain)
				.build();	

		this.testUser2Id = this.userRepository.findUserByUsername("test2").getId();			
	}

	@Test
	public void shouldGetPublicUserProfileByQuest() throws Exception {		 
		getPublicUserProfile(testUser2Id, null);			
	}

	@Test
	@Transactional
	public void shouldGetPublicUserProfileByOtherUser() throws Exception {
		String testUser1AuthoriazationHeader = TestUtils.authenticateTestUserAndReturnAuthorizationHeader(this.mockMvc, true);
		String username = "test999";
		String userEmail = "test999@emesch.com";		

		TestUtils.performRegisterUser(this.mockMvc, username, userEmail, "test123");
		Long newUserId = this.userRepository.findUserByUsername(username).getId();		

		getPublicUserProfile(newUserId, testUser1AuthoriazationHeader);		
	}

	@Test
	public void shouldNotGetPrivateUserProfileElements() throws Exception {
		String testUser1AuthoriazationHeader = TestUtils.authenticateTestUserAndReturnAuthorizationHeader(this.mockMvc, true);

		Assert.fail("Not implemented yet!");
	}



	@Test
	public void shouldGetInfoAboutNotExistingUser() throws Exception {
		String jwtTestUser = TestUtils.authenticateTestUserAndReturnAuthorizationHeader(this.mockMvc, true);
		tryGetNonExistingPublicUserProfile(NON_EXISTING_USER_ID, jwtTestUser);
		tryGetNonExistingPublicUserProfile(NON_EXISTING_USER_ID, null);
	}

	@Test
	@Transactional
	public void shouldCreateNewPublicUserProfileAfterCreatingNewUser() throws Exception {
		String username = "user999";		
		TestUtils.performRegisterUser(this.mockMvc, username, "testUser999@emesch.com", "test123");	
		long newUserId = this.userRepository.findUserByUsername(username).getId();

		getPublicUserProfile(newUserId, null);
		getPublicUserProfile(newUserId, TestUtils.authenticateTestUserAndReturnAuthorizationHeader(this.mockMvc, true));
	}

	@Test
	@Transactional
	public void shouldGetFriendsAndRelationsShouldBeOk() throws Exception {		
		String authorizationHeader = TestUtils.authenticateTestUserAndReturnAuthorizationHeader(this.mockMvc, true);		
		TestUtils.performRegisterUser(this.mockMvc, this.newTestUsername, this.newTestEmail, "test123");	
		long newUserId = this.userRepository.findUserByUsername(this.newTestUsername).getId();

		getFriendsOfUser(this.mockMvc, newUserId, authorizationHeader);				
	}	

	//@Test
	//@Transactional
	public void shouldSendInvitationToNewFriend() throws Exception {
		Assert.fail("Not implemented yet!");		
	}

	//@Test
	//@Transactional
	public void shouldAcceptInvitation() throws Exception {
		Assert.fail("Not implemented yet!");		
	}

	//@Test
	//@Transactional
	public void shouldNotInviteExistingFriend() throws Exception {
		Assert.fail("Not implemented yet!");		
	}

	//@Test
	//@Transactional
	public void shouldDeleteFriendFromFriendList() throws Exception {
		Assert.fail("Not implemented yet!");		
	}

	private void getPublicUserProfile(Long otherUserId, String authorizationHeader) throws Exception {
		MockHttpServletRequestBuilder req = TestUtils.prepareGetRequest(Api.getPublicUserProfile(otherUserId), authorizationHeader);
		this.mockMvc.perform(req)		
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasEntry(ResponseData.PARAM_VIEW, ResponseView.PUBLIC_PROFILE.name())));				

	}	

	private void getFriendsOfUser(MockMvc mockMvc, long otherUserId, String authorizationHeader) throws Exception {
		MockHttpServletRequestBuilder req = TestUtils.prepareGetRequest(Api.getFriendsOfUser(otherUserId), authorizationHeader);
		
		
		
		this.mockMvc.perform(req)		
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andDo(MockMvcResultHandlers.print());		
		
	}

	private void tryGetNonExistingPublicUserProfile(long nonExistingUserId, String authorizationHeader) throws Exception {
		MockHttpServletRequestBuilder req = TestUtils.prepareGetRequest(Api.getPublicUserProfile(nonExistingUserId), authorizationHeader);		

		this.mockMvc.perform(req)		
		.andExpect(MockMvcResultMatchers.status().isNotFound())
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasEntry(ResponseData.PARAM_VIEW, ResponseView.USER_NOT_FOUND.name())));	

	}	

}

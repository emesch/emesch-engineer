package emesch.edu.utils;

import java.io.IOException;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import emesch.edu.Api;
import emesch.edu.security.AuthenticationTest;
import emesch.edu.security.JwtAuthenticationRequest;
import emesch.edu.security.TokenObject;

public class TestUtils {

	/**
	 * Get jsonToken for testUser - token: xxxx
	 * Return authorization header with or without Bearer.  
	 */
	public static String authenticateTestUserAndReturnAuthorizationHeader(MockMvc mockMvc, boolean withBearer) throws Exception {			
		String jsonToken =  TestUtils.performLoginAndGetJsonToken(mockMvc, getJsonForTestUser());				
		return getAuthorizationHeaderFromJsonToken(jsonToken, withBearer);
	}
	
	public static MockHttpServletRequestBuilder prepareGetRequest(String uri, String authorizationHeader) {
		MockHttpServletRequestBuilder req = MockMvcRequestBuilders.get(uri);
		if(authorizationHeader!=null) { 
			req.header("Authorization", authorizationHeader);
		}
		return req;
	}

	/**
	 * Returns authorization header sent in requests. 
	 */
	public static String getAuthorizationHeaderFromJsonToken(String jsonToken, boolean withBearer) throws JsonParseException, JsonMappingException, IOException {
		String authHeader = new ObjectMapper().readValue(jsonToken, TokenObject.class).getToken();
		return withBearer?"Bearer "+authHeader : authHeader;
	}

	/**
	 * Do post to authentication path, sending JSON.
	 * Expecting: status OK. 
	 * Returning response content as String. 
	 */
	public static String performLoginAndGetJsonToken(MockMvc mockMvc, String json) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders
				.post(AuthenticationTest.AUTHENTICATION_PATH)
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))	
				.andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn().getResponse().getContentAsString();		
	}

	/**
	 * 	Create new JwtAuthenticationRequest for test user, and return it as JSON.
	 */
	public static String getJsonForTestUser() throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		JwtAuthenticationRequest authRequest = new JwtAuthenticationRequest("test", "testUser@tests.emesch.com", "test123");		
		String json = mapper.writeValueAsString(authRequest);
		return json;
	}

	public static void performRegisterUser(MockMvc mockMvc, String username, String userEmail, String userNonEncryptedPassword) throws Exception {
		mockMvc.perform(MockMvcRequestBuilders
				.post(Api.registerUser())
				.param("username", username)
				.param("email", userEmail)
				.param("password", userNonEncryptedPassword))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(MockMvcResultMatchers.status().isCreated());	
	}	

}

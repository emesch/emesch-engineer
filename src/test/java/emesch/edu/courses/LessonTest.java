package emesch.edu.courses;

import javax.servlet.Filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import emesch.edu.repositories.LessonRepository;
import emesch.edu.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class LessonTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;
	@Autowired
	private Filter springSecurityFilterChain;
	
	@Autowired
	private LessonRepository repository;

	@Before
	public void setup() {		
		this.mockMvc = MockMvcBuilders
				.webAppContextSetup(this.wac)
				.addFilters(this.springSecurityFilterChain)
				.build();			
	}
	
	@Test
	public void shouldCreateAndReturnLesson() {
		
	}

}
